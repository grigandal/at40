Dim CurDir
Dim bb

Sub SetGlobals(aCurDir, aBB)
  CurDir = aCurDir
  Set bb = aBB
End Sub 

Function VStr(Var)
  ' ���������� ���������� � ������
  If IsNull(Var) Then
    VStr = ""
  Else
    VStr = CStr(Var)
  End If
End Function

Sub ShowBB()
  bb.ShowObjectTree
End Sub

' ==========================================================================================================

Sub PutOnBB(nm, val)
  bb.GetChildCount "bb.wm.facts", "fact", count, exists 	'� count - ����� ���� ����� ��������
  bb.AddObject     "bb.wm.facts.fact["+CStr(count)+"]", exists
  bb.SetParamValue "bb.wm.facts.fact["+CStr(count)+"]", "AttrPath", nm, exists
  bb.SetParamValue "bb.wm.facts.fact["+CStr(count)+"]", "Value", val, exists
End Sub

Sub PrepareInput()
  Dim fs, f
  Set fs = CreateObject("Scripting.FileSystemObject")
  Set f = fs.CreateTextFile(CurDir + "input.txt", True)
  f.WriteLine (".MATRIX 0 0 2 1")
  f.WriteLine ("    8 ""dmitri""")
  f.Close
End Sub

Sub UseMCAD()
  '�������� ���� � ��������� �������
  'call PrepareInput()

  '�������� Mathcad � ��� ���������
  Dim App
  Set App = CreateObject("Mathcad.Application")
  App.Visible = true
  App.Worksheets.Open CurDir + "assessment.mcd"

  Dim Ncp, Mcp, Tcp, Tw
  Set Ncp = App.ActiveWorksheet.GetValue("Ncp")	'������� ����� ��������� � ����
  Set Mcp = App.ActiveWorksheet.GetValue("Mcp")	'������� ����� ��������� � �������
  Set Tcp = App.ActiveWorksheet.GetValue("Tcp")	'������� ����� ���������� ��������� � ����
  Set Tw = App.ActiveWorksheet.GetValue("Tw")	'������� ����� �������� ��������� � �������

  call PutOnBB("Ncp", Ncp.AsString)
  call PutOnBB("Mcp", Mcp.AsString)
  call PutOnBB("Tcp", Tcp.AsString)
  call PutOnBB("Tw", Tw.AsString)

  App.Quit

End Sub

