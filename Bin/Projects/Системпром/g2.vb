Dim CurDir
Dim bb

Sub SetGlobals(aCurDir, aBB)
  CurDir = aCurDir
  Set bb = aBB
End Sub 

Function VStr(Var)
  ' ���������� ���������� � ������
  If IsNull(Var) Then
    VStr = ""
  Else
    VStr = CStr(Var)
  End If
End Function

Sub ShowBB()
  bb.ShowObjectTree
End Sub

' ==========================================================================================================

' ������� �������������� ������� �������� � ����������, ����� G2 ��������
Function Translate(aStr)

  Dim rus_lxm, eng_lxm
  
  rus_lxm = Array(" ", "�", "�", "�", "�", "�", "�", "�",  "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",  "�",  "�",  "�",    "�", "�", "�", "�", "�",  "�",  "�", "�", "�", "�", "�", "�", "�",  "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�",  "�",  "�",  "�",    "�", "�", "�", "�", "�",  "�")
  eng_lxm = Array("_", "a", "b", "v", "g", "d", "e", "yo", "j", "z", "i", "y", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "ts", "ch", "sh", "shch", "'", "y", "'", "�", "yu", "ya", "A", "B", "V", "G", "D", "E", "Yo", "J", "Z", "I", "Y", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "Ts", "Ch", "Sh", "Shch", "'", "Y", "'", "�", "Yu", "Ya")

  i = 0
  Do While i <= 66
    aStr = Replace(astr, rus_lxm(i), eng_lxm(i))
    i = i + 1
  Loop 

  Translate = aStr

End Function


' ��������� �������� ������� � G2
Sub Export4G2()  
  ' �������� ������ � �������� ����� � �������� ��� � XML, ����� �� �������� � �������� ������
  Dim xml
  bb.GetXMLText "bb.prj", xml
  
  Dim XMLDoc
  Set XMLDoc = CreateObject("MSXML.DOMDocument")
  XMLDoc.loadXML xml  
  'XMLDoc.save CurDir + "bbprj.xml"

  ProjectName = XMLDoc.documentElement.getAttribute("ProjectName")  
  FileName = "stringtxt.txt"
  'MsgBox FileName

  ' ������� ���� ��� ��������
  Dim fs, f
  Set fs = CreateObject("Scripting.FileSystemObject")
  Set f = fs.CreateTextFile(CurDir + FileName, True)

  ' ���������� ������� G2
  BuildG2Proc XMLDoc.documentElement.firstChild, f

  f.Close

  MsgBox "����������� ���� stringtxt.txt"

  Translate("asdf")
End Sub


Sub BuildG2Proc(node, file)

  ind = 0
  aVars = "proc () "
  aCode = "begin "

  aCode = aCode + Chr(13) + Chr(10)

  RecurseInto node, file, ind, aVars, aCode

  aVars = aVars + Chr(13) + Chr(10)

  file.Write(aVars)
  file.Write(aCode)
  file.Write(" end")

End Sub 


Sub RecurseInto(node, file, ind, aVars, aCode)
  
  '������� ���������� � ������� ���� (������ ��� elem) 

  if node.getAttribute("type") = "elem" then
    ind = ind + 1
    lanID = "lan" + CStr(ind)
    lanName = """" + Translate(node.getAttribute("name")) + """"    

    aVars = aVars + Chr(13) + Chr(10)
    aVars = aVars + "  " + lanID + ": class tlan; "
    
    s = ""
    s = s + "  create a tlan " + lanID + "; " 
    s = s + Chr(13) + Chr(10)
    s = s + "  conclude that the lan_name of " + lanID + " = " + lanName + "; "
    s = s + Chr(13) + Chr(10)
    s = s + "  conclude that the workstation_number of " + lanID + " = 5; "
    s = s + Chr(13) + Chr(10)
    s = s + "  conclude that the transmission_delay of " + lanID + " = " + CStr(Int((10 * Rnd) + 1))  + "; "
    s = s + Chr(13) + Chr(10)
    s = s + "  transfer " + lanID + " to field at (" + CStr(ind * 50) + ", 150); "
    s = s + Chr(13) + Chr(10)

    aCode = aCode + s

  End If

  '��������� �� �������� �����
  i=0
  Dim child
  while i < node.childNodes.length
    Set child = node.childNodes.item(i)
    'MsgBox node.childNodes.item(i).nodeName

    '���������� ������ ���� node
    if child.nodeName = "node" then    
      RecurseInto child, file, ind, aVars, aCode
    end if  
  
    i = i+1
  wend    

End Sub


