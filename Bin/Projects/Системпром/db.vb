Dim CurDir
Dim bb

Sub SetGlobals(aCurDir, aBB)
  CurDir = aCurDir
  Set bb = aBB
End Sub 

Function VStr(Var)
  ' ���������� ���������� � ������
  If IsNull(Var) Then
    VStr = ""
  Else
    VStr = CStr(Var)
  End If
End Function

Sub ShowBB()
  bb.ShowObjectTree
End Sub


' ============================= ������� ���� =======================================

Function FormKIVSSQL(onetype, method, multisegment, conn_type, line_type,  line, star_line, star_ring) 

  s =     "SELECT a.[���], a.[������ ����], a.[������������ ���������� ���], b.[�������], "
  s = s + "a.[����� ���������� ���], a.[������������� ���������������� �������������], a.[���������], "
  s = s + "a.[��� ���������� ����������], a.[��� ���������� �����], b.[�������� �������] "
  s = s + "FROM [������� ����] a, [������� ��������� ����] b "
  s = s + "WHERE a.[��� �������] = b.[���] " 

  If onetype <> "" Then
    s = s + "AND a.[������������ ���������� ���] = '" + CStr(onetype) + "' "
  End If  

  If method <> "" Then
    s = s + "AND a.[����� ���������� ���] = '" + CStr(method) + "' "
  End If

  If multisegment <> "" Then
    s = s + "AND a.[������������� ���������������� �������������] = '" + CStr(multisegment) + "' "
  End If

  If conn_type <> "" Then
    s = s + "AND a.[��� ���������� ����������] = '" + CStr(conn_type) + "' "
  End If

  If line_type <> "" Then
    s = s + "AND a.[��� ���������� �����] = '" + CStr(line_type) + "' "
  End If

  putand = (star_line <> "") OR (star_ring <> "")
  putor = (star_line <> "") AND (star_ring <> "")

  If putand Then
    s = s + "AND ("

    If star_line <> "" Then
      s = s + "a.[���������] = '" + CStr(star_line) + "' "
    End If

    If putor Then
      s = s + "OR "
    End If

    If star_ring <> "" Then
      s = s + "a.[���������] = '" + CStr(star_ring) + "' "
    End If

    s = s + ") "
  End If

  s = s + "ORDER BY a.[���] "
  
  FormKIVSSQL = s
End Function


Sub ChooseKIVS(onetype, method, multisegment, conn_type, line_type,  line, star_line, star_ring)
 
  s = "���������� � ����." + Chr(13)
  s = s + "  ������������ ���������� ���: " + CStr(onetype) + Chr(13)
  s = s + "  ��������� ����: " + CStr(line) + " " + CStr(star_line) + " " + CStr(star_ring) + Chr(13)
  s = s + "  ����� ���������� ���: " + CStr(method) + Chr(13)
  s = s + "  ������������� ���������������� �������������: " + CStr(multisegment) + Chr(13)
  s = s + "  ��� ���������� ����������: " + CStr(conn_type) + Chr(13)
  s = s + "  ��� ���������� �����: " + CStr(line_type) + Chr(13)

  MsgBox s

  Dim Connection
  Set Connection = CreateObject("ADODB.Connection")
  Connection.ConnectionTimeOut = 15
  Connection.CommandTimeOut = 300
  Connection.Open "network", "", ""

  Dim RS
  Set RS = CreateObject("ADODB.Recordset")

  bb.RemoveObject "bb.wm.selection", Exists

  strSelect = FormKIVSSQL(onetype, method, multisegment, conn_type, line_type,  line, star_line, star_ring)
  'Msgbox strSelect

  RS.Open strSelect, Connection, 1, 1  

  If RS.EOF then 
    s = "���������� ����� ���� �� �������. " + Chr(13) + "����� ���������� �������������� ������� ��."
  Else 
    s = "���������� ����� ����: " + Chr(13)
    xml = "<node type='struct' name='���������� �����' add='F'>"    
    i = 0
    RS.MoveFirst()
    while not RS.EOF

      s = s + "  " + RS.Fields("������ ����").Value + Chr(13)

      xml = xml + "<node type='struct' name='" + RS.Fields("������ ����").Value + "' id='" + CStr(RS.Fields("���").Value) + "' select='T' add='F'>"
      xml = xml + "<node type='pic' name='�����' remove='F'>" + RS.Fields("�������").Value + "</node>"
      xml = xml + "<node type='table' name='��������������' remove='F'>"
      xml = xml + "<list name='��������������'>"
      xml = xml + "<text>������������ ���������� ���</text>"
      xml = xml + "<text>����� ���������� ���</text>"
      xml = xml + "<text>������������� ���������������� �������������</text>"
      xml = xml + "<text>��� ���������� ����������</text>"
      xml = xml + "<text>��� ���������� �����</text>"
      xml = xml + "<text>���������</text>"
      xml = xml + "</list>"
      xml = xml + "<list name='��������'>"      
      xml = xml + "<text>" + VStr(RS.Fields("������������ ���������� ���").Value) + "</text>"
      xml = xml + "<text>" + VStr(RS.Fields("����� ���������� ���").Value) + "</text>"            
      xml = xml + "<text>" + VStr(RS.Fields("������������� ���������������� �������������").Value) + "</text>"
      xml = xml + "<text>" + VStr(RS.Fields("��� ���������� ����������").Value) + "</text>"      
      xml = xml + "<text>" + VStr(RS.Fields("��� ���������� �����").Value) + "</text>"
      xml = xml + "<text>" + VStr(RS.Fields("���������").Value) + "</text>"
      xml = xml + "</list>"
      xml = xml + "</node>"
      xml = xml + "</node>"           

      RS.MoveNext()
      i = i+1
    wend    
    xml = xml + "</node>"

    bb.SetXMLText "bb.wm.selection", xml
  End If
  RS.Close

  Msgbox s, , "������ ������������"

  Connection.Close
  
  
  bb.GetXMLText "bb.wm.selection", xml

  'Msgbox "������� �������� �� �������� ����� � ������ bb.wm.selection. "+Chr(13)+"������ ���� bb.xml. � ��� � ��� XML: "+Chr(13)+xml

End Sub


Function FormKIVSContentSQL(KIVS_ID)
  s =     "SELECT a.[����� �����], a.[��� ������� ���], a.[���������� ���], b.[������ ���], c.[�������] "
  s = s + "FROM [����� ���� � ���] a, [������� ���] b, [������� ��������� ���] c "
  s = s + "WHERE a.[��� ������� ����] = " + CStr(KIVS_ID) 
  s = s + " and a.[��� ������� ���] = b.[���] and b.[��� �������] = c.[���] "
  s = s + "ORDER BY a.[����� �����] "

  FormKIVSContentSQL = s
End Function


Sub KIVSSelected(KIVS_ID)

  'MsgBox "KIVSSelected" + KIVS_ID

  Dim Connection
  Set Connection = CreateObject("ADODB.Connection")
  Connection.ConnectionTimeOut = 15
  Connection.CommandTimeOut = 300
  Connection.Open "network", "", ""

  Dim RS
  Set RS = CreateObject("ADODB.Recordset")

  bb.RemoveObject "bb.wm.kids", Exists

  strSelect = FormKIVSContentSQL(KIVS_ID)
  'Msgbox strSelect

  RS.Open strSelect, Connection, 1, 1  
 
  If not RS.EOF then 

    xml = "<node type='struct' name='������������ ��������'>"    
    i = 0
    RS.MoveFirst()
    while not RS.EOF
      num = RS.Fields("���������� ���").Value
      i = 0
      while i < num
        xml = xml + "<node type='elem' name='" + RS.Fields("������ ���").Value + " (" + CStr(i) + ")' id='" + CStr(RS.Fields("��� ������� ���").Value) + "' level='2' state='started'>"
        xml = xml + "<node type='pic' name='�����'>" + RS.Fields("�������").Value + "</node>"
        xml = xml + "</node>"
        i = i + 1
      wend

      RS.MoveNext()
      i = i+1
    wend    
    xml = xml + "</node>"

    bb.SetXMLText "bb.wm.kids", xml
  End If
  RS.Close

  Connection.Close  
  
  bb.GetXMLText "bb.wm.kids", xml

  'Msgbox "������� �������� �� �������� ����� � ������ bb.wm.kids. "+Chr(13)+"������ ���� bb.xml. � ��� � ��� XML: "+Chr(13)+xml

End Sub


' ============================= ������� ��� =======================================

Function FormLVSSQL(rank, technology, standard, method,  line, star, ring, double_ring) 

  s =     "SELECT a.[���], a.[������ ���], b.[�������], "
  s = s + "a.[���� ����], a.[������� ����������], a.[C����� ���������� ��������], "
  s = s + "a.[��������], a.[���������] "
  s = s + "FROM [������� ���] a, [������� ��������� ���] b "
  s = s + "WHERE a.[��� �������] = b.[���] " 

  If rank <> "" Then
    s = s + "AND a.[���� ����] = '" + CStr(onetype) + "' "
  End If

  If technology <> "" Then
    s = s + "AND a.[������� ����������] = '" + CStr(technology) + "' "
  End If

  If standard <> "" Then
    s = s + "AND a.[��������] = '" + CStr(standard) + "' "
  End If

  If method <> "" Then
    s = s + "AND a.[C����� ���������� ��������] = '" + CStr(method) + "' "
  End If


  putand = line <> "" OR star <> "" OR ring <> "" OR double_ring <> ""
  ok = false

  If putand Then
    s = s + "AND ("

    If line <> "" Then
      s = s + "a.[���������] = '" + CStr(line) + "' "
      ok = true
    End If

    If star <> "" Then
      If ok Then
        s = s + "OR "
      End If
      s = s + "a.[���������] = '" + CStr(star) + "' "
      ok = true
    End If

    If ring <> "" Then
      If ok Then
        s = s + "OR "
      End If
      s = s + "a.[���������] = '" + CStr(ring) + "' "
      ok = true
    End If

    If double_ring <> "" Then
      If ok Then
        s = s + "OR "
      End If
      s = s + "a.[���������] = '" + CStr(double_ring) + "' "
      ok = true
    End If

    s = s + ") "
  End If

  s = s + "ORDER BY a.[���] "
  
  FormLVSSQL = s
End Function


Sub ChooseLVS(rank, technology, standard, method,  line, star, ring, double_ring)
 
  s = "���������� � ���." + Chr(13)
  s = s + "  ���� ����: " + CStr(rank) + Chr(13)
  s = s + "  ������� ����������: " + CStr(technology) + Chr(13)
  s = s + "  �������� Ethernet: " + CStr(standard) + Chr(13)
  s = s + "  C����� ���������� ��������: " + CStr(method) + Chr(13)
  s = s + "  ��������� ����: " + CStr(line) + " " + CStr(star) + " " + CStr(ring) + " " + CStr(double_ring) + Chr(13)

  MsgBox s

  Dim Connection
  Set Connection = CreateObject("ADODB.Connection")
  Connection.ConnectionTimeOut = 15
  Connection.CommandTimeOut = 300
  Connection.Open "network", "", ""

  Dim RS
  Set RS = CreateObject("ADODB.Recordset")
  
  bb.RemoveObject "bb.wm.selection", Exists

  strSelect = FormLVSSQL(rank, technology, standard, method,  line, star, ring, double_ring)  

  'Msgbox strSelect

  RS.Open strSelect, Connection, 1, 1  

  If RS.EOF then 
    s = "���������� ����� ��� �� �������." + Chr(13) + "����� ���������� �������������� ��� ������� ��." 
  Else 
    s = "���������� ����� ���: " + Chr(13)    
    xml = "<node type='struct' name='���������� �����' add='F'>"    
    i = 0
    RS.MoveFirst()
    while not RS.EOF

      s = s + "  " + RS.Fields("������ ���").Value + Chr(13)

      xml = xml + "<node type='struct' name='" + RS.Fields("������ ���").Value + "' id='" + CStr(RS.Fields("���").Value) + "' select='T' add='F'>"
      xml = xml + "<node type='pic' name='�����' remove='F'>" + RS.Fields("�������").Value + "</node>"
      xml = xml + "<node type='table' name='��������������' remove='F'>"
      xml = xml + "<list name='��������������'>"
      xml = xml + "<text>���� ����</text>"
      xml = xml + "<text>������� ����������</text>"
      xml = xml + "<text>��������</text>"
      xml = xml + "<text>C����� ���������� ��������</text>"
      xml = xml + "<text>���������</text>"
      xml = xml + "</list>"
      xml = xml + "<list name='��������'>"      
      xml = xml + "<text>" + VStr(RS.Fields("���� ����").Value) + "</text>"
      xml = xml + "<text>" + VStr(RS.Fields("������� ����������").Value) + "</text>"            
      xml = xml + "<text>" + VStr(RS.Fields("��������").Value) + "</text>"
      xml = xml + "<text>" + VStr(RS.Fields("C����� ���������� ��������").Value) + "</text>"      
      xml = xml + "<text>" + VStr(RS.Fields("���������").Value) + "</text>"
      xml = xml + "</list>"
      xml = xml + "</node>"
      xml = xml + "</node>"           

      RS.MoveNext()
      i = i+1
    wend    
    xml = xml + "</node>"

    bb.SetXMLText "bb.wm.selection", xml
      
  End If
  RS.Close

  Msgbox s, , "������ ������������"

  Connection.Close
  
  Dim xml
  bb.GetXMLText "bb.wm.selection", xml

  'Msgbox "������� �������� �� �������� ����� � ������ bb.wm.selection. "+Chr(13)+"������ ���� bb.xml. � ��� � ��� XML: "+Chr(13)+xml

End Sub


Function FormLVSContentSQL(LVS_ID)
  s =     "SELECT a.[������], a.[������������], a.[����������], a.[���� ����������], a.[��� ������] "
  s = s + "FROM [������������ ���] a "
  s = s + "WHERE a.[��� �������] = " + CStr(LVS_ID) + " "
  s = s + "ORDER BY a.[������] "  

  FormLVSContentSQL = s
End Function

Sub LVSSelected(LVS_ID)

  'MsgBox "LVSSelected " + LVS_ID

  ' ����� ����������� ����� � ������2.�������1
  bb.FindObject "bb.wm.facts.fact", "AttrPath", "������2.�������1", Index
  Number = 0
  Override = false
  If Index > -1 Then
    bb.GetParamValue "bb.wm.facts.fact["+VStr(Index)+"]", "Value", Number, Exists 
    If Number > 0 Then
      Override = true
    End If
  End If
  'MsgBox Number

  Dim Connection
  Set Connection = CreateObject("ADODB.Connection")
  Connection.ConnectionTimeOut = 15
  Connection.CommandTimeOut = 300
  Connection.Open "network", "", ""

  Dim RS
  Set RS = CreateObject("ADODB.Recordset")

  bb.RemoveObject "bb.wm.kids", Exists

  strSelect = FormLVSContentSQL(LVS_ID)
  'Msgbox strSelect

  RS.Open strSelect, Connection, 1, 1  
 
  If not RS.EOF then 

    xml = "<node type='struct' name='������������ ��������'>"    
    i = 0
    RS.MoveFirst()
    while not RS.EOF
      num = RS.Fields("����������").Value

      If (RS.Fields("���� ����������").Value = "������� �������") and (Override = true) Then
        num = Number*1
      End If

      i = 1
      while i <= num
        txt = RS.Fields("������������").Value 

        if txt = "���������" Then
          txt = txt + " - " + RS.Fields("���� ����������").Value
        End If
        if txt = "������" Then
          txt = txt + " - " + RS.Fields("��� ������").Value
        End If

        xml = xml + "<node type='elem' name='" + txt + " (" + CStr(i) + ")' level='3' state='finished'>"
        xml = xml + "</node>"
        i = i + 1
      wend

      RS.MoveNext()
      i = i+1
    wend    
    xml = xml + "</node>"

    bb.SetXMLText "bb.wm.kids", xml
  End If
  RS.Close

  Connection.Close  
  
  bb.GetXMLText "bb.wm.kids", xml

  'Msgbox "������� �������� �� �������� ����� � ������ bb.wm.kids. "+Chr(13)+"������ ���� bb.xml. � ��� � ��� XML: "+Chr(13)+xml

End Sub


' ============================= ������� ������������ =======================================

Function FormHubSQL(ethernet, nway, security, ports, VPN, firewall)
  strSelect = "select * from Hub"
  
  If ethernet Or nway Or security Or ports Then
    strSelect = strSelect + " where "
  End If

  If ethernet Then
    strSelect = strSelect + "FastEthernet=" + ethernet
    putand = true
  End If

  If nway Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "nway=" + nway
    putand = true
  End If

  If security Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "high_security=" + security
  End If
  
  If ports Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "ports>8"
  End If

  FormHubSQL = strSelect
End Function


Function FormRouterSQL(ethernet, nway, security, ports, VPN, firewall)
  strSelect = "select * from Router"
  
  If firewall Or VPN Then
    strSelect = strSelect + " where "
  End If

  If firewall Then
    strSelect = strSelect + "firewall=" + firewall
    putand = true
  End If

  If VPN Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "VPN=" + VPN
    putand = true
  End If  
  
  FormRouterSQL = strSelect
End Function


Function FormDevicesSQL(AnalogID) 

  s =     "SELECT * "
  s = s + "FROM [������� ������������] a, [������������ ���] b "
 
  FormDevicesSQL = s
End Function


Sub ChooseHubRouter(ethernet, nway, security, ports, VPN, firewall)

  Dim Connection
  Set Connection = CreateObject("ADODB.Connection")
  Connection.ConnectionTimeOut = 15
  Connection.CommandTimeOut = 300
  Connection.Open "network", "", ""

  Dim RS
  Set RS = CreateObject("ADODB.Recordset")

  call bb.RemoveObject("bb.wm.selection", exists)

  s = "��������� ������������:" + Chr(13) + Chr(13)

  strSelect = FormHubSQL(ethernet, nway, security, ports, VPN, firewall)  
  'Msgbox strSelect

  RS.Open strSelect, Connection, 1, 1  
  s = s + "�����������/�������������: " + Chr(13)
  i = 0

  If RS.EOF then 
    s = s + "  �� �������. ���������� �������� ����������." + Chr(13)
  Else 
    RS.MoveFirst()
    while not RS.EOF
      call bb.AddObject("bb.wm.selection.record", exists)
      bb.SetParamValue "bb.wm.selection.record["+CStr(i)+"]", "name", RS.Fields("name").Value, exists
      bb.SetParamValue "bb.wm.selection.record["+CStr(i)+"]", "type", RS.Fields("type").Value, exists
      bb.SetParamValue "bb.wm.selection.record["+CStr(i)+"]", "FastEthernet", RS.Fields("FastEthernet").Value, exists
      bb.SetParamValue "bb.wm.selection.record["+CStr(i)+"]", "NWay", RS.Fields("nway").Value, exists
      bb.SetParamValue "bb.wm.selection.record["+CStr(i)+"]", "HighSecurity", RS.Fields("High_Security").Value, exists
    
      s = s + "  " + RS.Fields("type").Value + " " + RS.Fields("name").Value + " �� ���� $" + CStr(RS.Fields("price").Value) + Chr(13)
      RS.MoveNext()
      i = i+1
    wend    
  End If
  RS.Close

  '�������
  strSelect = FormRouterSQL(ethernet, nway, security, ports, VPN, firewall)  
  'Msgbox strSelect

  RS.Open strSelect, Connection, 1, 1  
  s = s + Chr(13) + "�������: " + Chr(13)

  If RS.EOF then 
    s = s + "  �� �������. ���������� �������� ����������."
  Else 
    RS.MoveFirst()
    while not RS.EOF
      call bb.AddObject("bb.wm.selection.record", exists)
      bb.SetParamValue "bb.wm.selection.record["+CStr(i)+"]", "name", RS.Fields("name").Value, exists
      bb.SetParamValue "bb.wm.selection.record["+CStr(i)+"]", "type", RS.Fields("type").Value, exists
      bb.SetParamValue "bb.wm.selection.record["+CStr(i)+"]", "firewall", RS.Fields("firewall").Value, exists
      bb.SetParamValue "bb.wm.selection.record["+CStr(i)+"]", "VPN", RS.Fields("VPN").Value, exists
    
      s = s + "  " + RS.Fields("type").Value + " " + RS.Fields("name").Value + " �� ���� $" + CStr(RS.Fields("price").Value) + Chr(13)
      RS.MoveNext()
      i = i+1
    wend
  End If
  RS.Close

  Msgbox s, , "������ ������������"

  Connection.Close
  
  Dim xml
  bb.GetXMLText "bb.wm.selection", xml

  Msgbox "������� �������� �� �������� ����� � ������ bb.wm.selection. "+Chr(13)+"������ ���� bb.xml. � ��� � ��� XML: "+Chr(13)+xml

End Sub


Sub ChooseDevices() 
  
End Sub


Sub DeviceSelected()
End Sub
