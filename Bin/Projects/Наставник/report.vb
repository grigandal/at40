Dim CurDir
Dim bb

Sub SetGlobals(aCurDir, aBB)
  CurDir = aCurDir
  Set bb = aBB
End Sub 

'Sub ShowBB()
 ' bb.ShowObjectTree()
'End Sub 

Sub Form(task, answer, simple, inter, evm, res1, price, exp, newexp, mest, usl, res2, deist, verb, spec, edin, days, resh, res3, final,pon, otn, rass, glub, jest, res4, syst, podzad, sdacha, razmer, comand, res5, name)
  Dim App
  Set App = CreateObject("Excel.Application")  
  App.Workbooks.Open(CurDir + "report.xls")

  'App.Range("B2").Select
  'App.ActiveCell.Value = name
  App.Range("C2").Select
  App.ActiveCell.Value = CStr(Date())

  App.Range("B4").Select
  if task = "��������" then     
    App.ActiveCell.Value = "������� ������ ��������� �� ������������� �������� � �������"
  else
    App.ActiveCell.Value = "������� ������ ��������� �� ������������� �������� � ���������"
  end if

  App.Range("B5").Select
  if answer = "������ ��������" then 
    App.ActiveCell.Value = "������ ����� ������ ��������������� �������"
  else
    App.ActiveCell.Value = "������� ������ ��������� �� ������������� ���������"
  end if

  App.Range("B6").Select
  if simple = "������ ������" then 
    App.ActiveCell.Value = "������� ������ �� ������� ������"
  end if

  if simple = "������ �� ������� ��� ����" then 
    App.ActiveCell.Value = "������ �� ������� ������"
  end if

  if simple = "��� ������ �� ��������� �� ������" then 
    App.ActiveCell.Value = "������, ��������, ������� ������"
  end if

  App.Range("B7").Select
  if inter = "��" then 
    App.ActiveCell.Value = "������ ������������ ������� ��� ��������"
  end if

  if inter = "������ ��, ��� ���" then 
    App.ActiveCell.Value = "������, ������ �����, ������������ ������� ��� ��������"
  end if

 if inter = "������ ���, ��� ��" then 
    App.ActiveCell.Value = "������, ��������, ������������ ������� ��� ��������"
  end if

if inter = "���" then 
    App.ActiveCell.Value = "������ �� ������������ �������� ��� ��������"
  end if


App.Range("B8").Select
  if evm = "��������" then 
    App.ActiveCell.Value = "������ �� �������� ������� ������� ��� ������� �� ���"
  end if

  if evm = "����������" then 
    App.ActiveCell.Value = "������ �������� ������� ������� ��� ������� �� ���"
  end if

 if evm = "� ���� ��� �������������" then 
    App.ActiveCell.Value = "��� ������������� ������������ ��� ��� ������� ������ ������"
  end if


App.Range("B10").Select
  if res1 = "���������� ��� �������" then 
    App.ActiveCell.Value = "���������� ��� ������� ��� ������� ������ ������"
  end if

  if res1 = "���������� ��� �� �������" then 
    App.ActiveCell.Value = "���������� ��� �� ������� ��� ������� ������ ������"
  end if

 if res1 = "���������� ��� �������, ���� � � ����� �����" then 
    App.ActiveCell.Value = "���������� ��� ������� ��� ������� ������ ������ � ��������� ����� �����"
  end if

  
App.Range("B13").Select
  if price<20000 then     
    App.ActiveCell.Value = "������� ������ �� ������� ��������� �������� ������"
  end if

  if price>=20000 and price<100000 then     
    App.ActiveCell.Value = "������� ������ ������� ��������� �� ����� ������� �����"
  end if

  if price>=100000 then     
    App.ActiveCell.Value = "������� ������ ������� ��������� ������� �����"
  end if


  App.Range("B14").Select
  if exp<3  then 
    App.ActiveCell.Value = "��������� � ������ ������� ���� ������������"
  end if

if exp>=3 and  exp<=10 then 
    App.ActiveCell.Value = "��������� � ������ �������, � ��������, ����������"
  end if

if exp>10 then 
    App.ActiveCell.Value = "���������� ���������� ��������� � ������ �������"
  end if


  App.Range("B15").Select
  if newexp<2 then 
    App.ActiveCell.Value = "���������� ��������� ������������ ����������� ����� ������� ����� � ������ �������"
  end if

  if newexp>=2 and newexp<=3 then 
    App.ActiveCell.Value = "��������, ���������� ��������� ������������ ����������� ����� ������� ����� � ������ �������"
  end if

  if newexp>3 then 
    App.ActiveCell.Value = "��������� ������������ ����������� ����� ������� ����� � ������ ������� ���"
  end if

  App.Range("B16").Select
  if mest>=5 then 
    App.ActiveCell.Value = "������� ����������� ����� �� ������ ��������� ����������� ������"
  end if

  if mest>=3 and mest<5 then 
    App.ActiveCell.Value = "������� ����������� ����� � ���������� ��������� ����������� ������"
  end if

 if mest<3 then 
    App.ActiveCell.Value = "������� ����������� ����� � �������� ��������� ����������� ������"
  end if


App.Range("B17").Select
  if usl = "���������� ����������" then 
    App.ActiveCell.Value = "�������, � ������� �������� ������, ��������� ��� ��������"
  else App.ActiveCell.Value = "�������, � ������� �������� ������, ������ ��� ��������"
  end if


App.Range("B19").Select
  if res2 = "���������� ��� ���������" then 
    App.ActiveCell.Value = "���������� ��� ��������� ��� ������� ������ ������"
  end if

  if res2 = "���������� ��� �� ���������" then 
    App.ActiveCell.Value = "���������� ��� �� ��������� ��� ������� ������ ������"
  end if

 if res2 = "���������� ��� ���������, ���� � � ����� �����" then 
    App.ActiveCell.Value = "���������� ��� ��������� ��� ������� ������ ������ � ��������� ����� �����"
  end if



App.Range("B22").Select
  if deist = "�������" then     
    App.ActiveCell.Value = "������� ������ ������� �� ������ ���������������� �������, �� � ��������"
  else
    App.ActiveCell.Value = "������� ������ ������� ������ ���������������� �������"
  end if

  App.Range("B23").Select
  if verb = "��, ������ ������������" then 
    App.ActiveCell.Value = "�������� ����� ������� ����������� ��� ������ ������ � ��������� ��"
  else
    App.ActiveCell.Value = "�������� �� ����� ������� ����������� ��� ������ ������"
  end if

  App.Range("B24").Select
  if spec = "��, ����������� ����" then 
    App.ActiveCell.Value = "���������� ���������� ����������� �� ������� �������� �����"
  else
    App.ActiveCell.Value = "���������� ������������ �� ������� �������� ����� �� ����������"
  end if
  

  App.Range("B25").Select
  if edin = "��, �������� ����������" then 
    App.ActiveCell.Value = "�������� ���������� � ����������� ��� ��������"
   else
    App.ActiveCell.Value = "�������� �� ���������� � ����������� ��� ��������"
  end if

 

App.Range("B26").Select
  if days<3 then 
    App.ActiveCell.Value = "������ �� ������� ������"
  end if

  if days>=3 and days<=7 then 
    App.ActiveCell.Value = "�������� ��������� �� ����� ������ ��� ������� ������"
  end if

 if days>7 then 
    App.ActiveCell.Value = "������, ��������, ������� ������"
  end if


App.Range("B27").Select
  if resh = "������� ��������" then 
    App.ActiveCell.Value = "�������� ������ ���������� �������"
   else
    App.ActiveCell.Value = "������ ������� ���������� ����� ������� �������"
  end if



App.Range("B29").Select
  if res3 = "���������� ��� ��������" then 
    App.ActiveCell.Value = "���������� ��� �������� ��� ������� ������ ������"
  end if

  if res3 = "���������� ��� �� ��������" then 
    App.ActiveCell.Value = "���������� ��� �� �������� ��� ������� ������ ������"
  end if

 if res3 = "���������� ��� ��������, ���� � � ����� �����" then 
    App.ActiveCell.Value = "���������� ��� �������� ��� ������� ������ ������ � ��������� ����� �����"
  end if


App.Range("B31").Select
  if final = "���������� ��� ��������� ��� ��������������� ������" then 
    App.ActiveCell.Value = "���������� ��� ��������� ��� ������� ��������������� ������"
  end if

  if final = "���������� ��� �� ��������� ��� ������� ��������������� ������" then 
    App.ActiveCell.Value = "���������� ��� �� ��������� ��� ������� ��������������� ������"
  end if

 if final = "���������� ��� ��������� ��� ������� ��������������� ������, ���� � � ����� �����" then 
    App.ActiveCell.Value = "���������� ��� ��������� ��� ������� ��������������� ������ � ��������� ����� �����"
  end if


App.Range("B36").Select
  if pon = "�������" then     
    App.ActiveCell.Value = "������� ���������� ������� �������"
  end if

 if pon = "�������" then     
    App.ActiveCell.Value = "������� ���������� ������� �������"
  end if

if pon = "������ �������" then     
    App.ActiveCell.Value = "��������� ������� ���������� ������� �� ����"
  end if


  App.Range("B37").Select
  if otn = "���������� �� ����� ���������� ����������" then 
    App.ActiveCell.Value = "��������� ����� ��������� ���������� �� ����� ���������� ����������"
  end if

if otn = "������� ��������� ����� ��������� �� ��������" then 
    App.ActiveCell.Value = "��������� ����� ��������� ���������� ������� �������"
  end if

if otn = "������� ������� ����� ��������� �� ��������" then 
    App.ActiveCell.Value = "��������� ����� ��������� ���������� ������� �����"
  end if


  App.Range("B38").Select
  if rass = "�����������" then 
    App.ActiveCell.Value = "������ ����������� �����������"
  end if

  if rass = "�����������" then 
    App.ActiveCell.Value = "������ ����������� �����������"
  end if

  if rass = "���������� �������" then 
    App.ActiveCell.Value = "������ ����������� - ���������� �������"
  end if

  if rass = "����������� �� ��������" then 
    App.ActiveCell.Value = "������ ����������� - �� ��������"
  end if


  App.Range("B39").Select
  if glub = "�������������" then 
    App.ActiveCell.Value = "������ �������� ��������������"
  else
    App.ActiveCell.Value = "������ �������� ����������"
  end if

  
App.Range("B40").Select
  if jest = "�������" then 
    App.ActiveCell.Value = "������ �������� ��������"
  else
    App.ActiveCell.Value = "������ �������� �������"
  end if

  

App.Range("B42").Select
  if res4 = "������������� ������������ ���������� ������ ������������� ������" then 
    App.ActiveCell.Value = "������������� ������������ ���������� ������ ������������� ������"
  end if

  if res4 = "������������� ������������ ������������� ������ ������������� ������" then 
    App.ActiveCell.Value = "������������� ������������ ������������� ������ ������������� ������"
  end if

 if res4 = "������������� ������������ ����������� ������ ������������� ������" then 
    App.ActiveCell.Value = "������������� ������������ ����������� ������ ������������� ������"
  end if

  if res4 = "������������� ������������ �������������-�������� ������" then 
    App.ActiveCell.Value = "������������� ������������ �������������-�������� ������ ������������� ������"
  end if

if res4 = "������������� ������������ ��������� ������" then 
    App.ActiveCell.Value = "������������� ������������ ��������� ������ ������������� ������"
  end if

if res4 = "������������� ������������ ������� ������" then 
    App.ActiveCell.Value = "������������� ������������ ������� ������ ������������� ������"
  end if

if res4 = "���������� ��������� �����������" then 
    App.ActiveCell.Value = "� ���������, ���������� ��������� ��������� �� �������"
  end if

if res4 = "������������� ������������ ������ � ���� ������������� ����� ��� �������" then 
    App.ActiveCell.Value = "������������� ������������ ������ � ���� ������������� ����� ��� �������"
  end if


App.Range("B78").Select
  if syst = "��������� ����������� ��������" then     
    App.ActiveCell.Value = "��������������� ������� ������ ��������� ����������� ��������"
  end if

 if syst = "��������� ���������� ��������" then     
    App.ActiveCell.Value = "��������������� ������� ������ ��������� ���������� ��������"
  end if

if syst = "���� ������" then     
    App.ActiveCell.Value = "��������������� ������� ������ ���� ������"
  end if


  App.Range("B79").Select
  if podzad = "������ ��������������" then 
    App.ActiveCell.Value = "� ������� ���������� ������ ������ �������������� ���������"
  end if

if podzad = "����� ��������������" then 
    App.ActiveCell.Value = "� ������� ���������� ������ ����� �������������� ���������"
  end if

if podzad = "�� ����" then 
    App.ActiveCell.Value = "�������������� �������� ������� �� ����"
  end if


  App.Range("B80").Select
  if sdacha = "�� ����������� ��������" then 
    App.ActiveCell.Value = "����� � ������� ������������ ����������� �� ����������� ��������"
  end if

  if sdacha = "���� ������� �������" then 
    App.ActiveCell.Value = "����� � ������� ������������ ����������� ���� ������� �������"
  end if

   if sdacha = "��� �� ��������" then 
    App.ActiveCell.Value = "��� �� ��������, ����� ������� ����������� ����� � ������� ������������"
  end if

  

  App.Range("B81").Select
  if razmer = "��������� ������" then 
    App.ActiveCell.Value = "������ ���������"
  end if


if razmer = "������� ������ � �����������" then 
    App.ActiveCell.Value = "������ �������, � �����������"
  end if
  
if razmer = "�� ���� �������" then 
    App.ActiveCell.Value = "������� ������� �� ����"
  end if

App.Range("B82").Select
  if comand = "��, ����� ���������� ����" then 
    App.ActiveCell.Value = "� ������� ������������� ������� ��������� ������� �� ���������"
  end if


if comand = "���, ������� �������� � ������ ��������" then 
    App.ActiveCell.Value = "������� ������������� �������� � ������ ��������"
  end if
  

if comand = "������� �� ���������� ������" then 
    App.ActiveCell.Value = "������������ ������ ������� ������������� ������� �� ���������� ������"
  end if

App.Range("B84").Select
  if res5 = "��������� ��������" then 
    App.ActiveCell.Value = "������������� �������� ���������� ��������� ��� ���� ������ � �����,"
  end if

  if res5 = "��������� ��������" then 
    App.ActiveCell.Value = "������������� �������� ���������� ���������, ��� ������������ � ����������"
  end if

  if res5 = "��������� ����������" then 
    App.ActiveCell.Value = "������������� �������� ���������� �� ������ �� ��������, �� ����������� ������������,"
  end if



App.Range("B85").Select
  if res5 = "��������� ��������" then 
    App.ActiveCell.Value = "��� ������������ � ������� ������� �� ����� ����������� ����������"
  end if

  if res5 = "��������� ��������" then 
    App.ActiveCell.Value = "����������� ���������� �� ���� ����������� ���������� ������ �� ��������"
  end if

  if res5 = "��������� ����������" then 
    App.ActiveCell.Value = "� ����� ����������� � ������ ��������� ��������, �������� ��� ������ � �����"
  end if


 App.Range("B90").Select
   App.ActiveCell.Value = "�������: ADS  �������������: Aion Corp., USA"
  
 App.Range("B91").Select
   App.ActiveCell.Value = "�������: Gold Works  �������������: Gold Hill Computers, USA"
  
 App.Range("B92").Select
   App.ActiveCell.Value = "�������: KDS3  �������������: KDS Corp., USA"
  
 App.Range("B93").Select
   App.ActiveCell.Value = "�������: KES  �������������: SoftWare A&E, Inc, UK"
  
 App.Range("B94").Select
   App.ActiveCell.Value = "�������: M.1  �������������: Teknowledge, USA"
  
 App.Range("B95").Select
   App.ActiveCell.Value = "�������: RBEST  �������������: TitanSystems Inc., USA"
  


  App.Range("A1").Select
  App.Visible = true
  App = ""
  'App.Quit
End Sub