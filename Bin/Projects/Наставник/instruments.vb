Dim CurDir
Dim bb

Sub SetGlobals(aCurDir, aBB)
  CurDir = aCurDir
  Set bb = aBB
End Sub 

Sub ShowBB()
  bb.ShowObjectTree()
End Sub

Function FormSQL(logic, product, freim, forward, backward, induction, demons, otlad, menu, window, db, programs, equipment, c, lisp, pascal, prolog, ibmpc, sun, macintosh)
  strSelect = "select * from Instruments"
  
  If logic or product or freim or forward or backward or induction or demons or otlad or menu or window or db or programs or equipment or c or lisp or pascal or prolog or ibmpc or sun or macintosh Then
    strSelect = strSelect + " where "
  End If

If logic Then
    strSelect = strSelect + "logic='+'"
    putand = true
  End If

  If product Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "product='+'" 
    putand = true
  End If

  If freim Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "freim='+'" 
    putand = true
  End If


  If forward Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "forward='+'"
    putand = true
  End If

  If backward Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "backward='+'" 
    putand = true
  End If

  If induction Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "induction='+'"
  End If
  
  If demons Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "demons='+'"
  End If

 
If otlad Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "otlad='+'"
  End If

If menu Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "menu='+'"
  End If

If window Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "window='+'"
  End If

If db Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "db='+'"
  End If

If programs Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "programs='+'"
  End If

If equipment Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "equipment='+'"
  End If

If c Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "c=" + c
    putand = true
  End If

If lisp Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "lisp=" + lisp
    putand = true
  End If

If pascal Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "pascal=" + pascal
    putand = true
  End If

If prolog Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "prolog=" + prolog
    putand = true
  End If

If ibmpc Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "ibmpc=" + ibmpc
    putand = true
  End If

If sun Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "sun=" + sun
    putand = true
  End If


If macintosh Then
    If putand Then
      strSelect = strSelect + " and "
    End If
    strSelect = strSelect + "macintosh=" + macintosh
    putand = true
  End If

 FormSQL = strSelect
End Function


Sub UseODBC(logic, product, freim, forward, backward, induction, demons, otlad, menu, window, db, programs, equipment, c, lisp, pascal, prolog, ibmpc, sun, macintosh)
  Dim Connection
  Set Connection = CreateObject("ADODB.Connection")
  Connection.ConnectionTimeOut = 15
  Connection.CommandTimeOut = 300
  Connection.Open "DSN=instruments", "", ""
  

  Dim RS
  Set RS = CreateObject("ADODB.Recordset")

  call bb.RemoveObject("bb.selection", exists)
  i = 0
  s = "��������� ���������������� ��������:" + Chr(13) + Chr(13)

  strSelect = FormSQL(logic, product, freim, forward, backward, induction, demons, otlad, menu, window, db, programs, equipment, c, lisp, pascal, prolog, ibmpc, sun, macintosh) 
  
  RS.Open strSelect, Connection, 1, 1  

  If RS.EOF then 
    s = s + "  �� �������. ���������� �������� ����������." + Chr(13)
  Else 
    RS.MoveFirst()
    while not RS.EOF
      call bb.AddObject("bb.selection.record", exists)
      bb.SetParamValue "bb.selection.record["+CStr(i)+"]", "system", RS.Fields("system").Value, exists
      bb.SetParamValue "bb.selection.record["+CStr(i)+"]", "producer", RS.Fields("producer").Value, exists
      bb.SetParamValue "bb.selection.record["+CStr(i)+"]", "realislang", RS.Fields("realislang").Value, exists
          
      s = s + " ������� " + RS.Fields("system").Value + " �������������: " + RS.Fields("producer").Value + " ���� ����������: " + CStr(RS.Fields("realislang").Value) + Chr(13)
      RS.MoveNext()
      i = i+1
    wend    
  End If
  RS.Close

  

  'Msgbox s, , "������ ����������������� ��������"

  Connection.Close
  
  Dim xml
  bb.GetXMLText "bb.selection", xml

  'Msgbox "������� �������� �� �������� ����� � ������ bb.selection. "+Chr(13)+"������ ���� bb.xml. � ��� � ��� XML: "+Chr(13)+xml

End Sub

