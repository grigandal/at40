<format_file>
  <part file_ext="dsf" encoding="">
     <loop_rules>
        <rule_loop_asser_then>
          <add_to_list list_name="In Rules" pos="">
           <sassertion_object>
              <object_comment/>
              <text>.</text>
           </sassertion_object>
           <nassertion_object>
              <object_comment/>
              <text>.</text>
           </nassertion_object>
           <sassertion_attribute>
              <attribute_comment/>
              <text> (</text>
           </sassertion_attribute>
           <nassertion_attribute>
              <attribute_comment/>
              <text> (</text>
           </nassertion_attribute>
           <sassertion_object>
              <object_name/>
              <text>.</text>
           </sassertion_object>
           <nassertion_object>
              <object_name/>
              <text>.</text>
           </nassertion_object>
           <sassertion_attribute>
              <attribute_name/>
              <text>)</text>
           </sassertion_attribute>
           <nassertion_attribute>
              <attribute_name/>
              <text>)</text>
           </nassertion_attribute>
          </add_to_list>
        </rule_loop_asser_then>
     </loop_rules>

     <loop_objects>
        <object_loop_attributes>

<comment>
  ���� ���� ������� ������ ���� ����������, �������
  ���� � ���� ������
</comment>
            <add_to_list list_name="AllVariables" pos="">
               <object_name/>
               <text>.</text>
               <attribute_name/>
            </add_to_list>
<comment>
  ����� �����.
  ���� ���� ������� ������ ���� ����������, �������
  ���� � ���� ������
</comment>

            <add_to_list list_name="Not in Rules" pos="">
               <object_comment/>
               <text>.</text>
               <attribute_comment/>
               <text> (</text>
               <object_name/>
               <text>.</text>
               <attribute_name/>
               <text>)</text>
            </add_to_list>
            <while>
               <do>
                  <change>
                     <first>
                        <get_element_from_list list_name="Not in Rules" pos=""/>
                     </first>
                     <equalfirst>
                        <get_element_from_list list_name="In Rules" pos=""/>
                     </equalfirst>
                     <then>
                        <delete_from_list list_name="Not in Rules" pos=""/>
                        <add_to_list list_name="Temp In Rules" pos="">
                           <get_element_from_list list_name="In Rules" pos=""/>
                        </add_to_list>
                        <delete_from_list list_name="In Rules" pos=""/>
                     </then>
                     <else>
                        <add_to_list list_name="Temp In Rules" pos="">
                           <get_element_from_list list_name="In Rules" pos=""/>
                        </add_to_list>
                        <delete_from_list list_name="In Rules" pos=""/>
                     </else>
                  </change>
               </do>
               <condition equal="false">
                  <first/>
                  <equalfirst>
                     <count_elements_list list_name="In Rules"/>
                  </equalfirst>
               </condition>
            </while>

            <while>
               <do>
                 <add_to_list list_name="In Rules" pos="">
                    <get_element_from_list list_name="Temp In Rules" pos=""/>
                 </add_to_list>
                 <delete_from_list list_name="Temp In Rules" pos=""/>
               </do>
               <condition equal="false">
                  <first/>
                  <equalfirst>
                     <count_elements_list list_name="Temp In Rules"/>
                  </equalfirst>
               </condition>
            </while>
        </object_loop_attributes>
     </loop_objects>


     <dialog_message caption="�������� ��������� ��� ���������..." type="question" result_list_name="Create message for attributes">
        <text_box align="center" bold="">
          <text>�������� ��������, ��� ������� ����� ������� ���������� ����������</text>
        </text_box>
        <input_box caption="������ �������� ����� ���� �������� � �������� ������" type="enum_many" list_name="In Rules" default_number=""/>
        <input_box caption="������ �������� �� ����� ���� �������� � �������� ������" type="enum_many" list_name="Not in Rules" default_number="all"/>
     </dialog_message>


     <dialog_message caption="�������� ��������� � ������������� �����������..." type="question" result_list_name="Create subresults for attributes">
        <text_box align="center" bold="">
          <text>�������� ��������, ��� ������� ����� ������� ��������� � ������������� �����������</text>
        </text_box>
        <input_box caption="������ �������� ����� ���� �������� � �������� ������" type="enum_many" list_name="In Rules" default_number="all"/>
        <input_box caption="������ �������� �� ����� ���� �������� � �������� ������" type="enum_many" list_name="Not in Rules" default_number=""/>
     </dialog_message>


<comment>
  ���� ���� ������� ������ ������������(��� �������������)
  � �� ������������(��� �� �������������) ���������
</comment>
     <while>
        <do>
           <loop_objects>
              <object_loop_attributes>
                 <change>
                    <first>
                       <get_element_from_list list_name="Create message for attributes" pos=""/>
                    </first>
                    <equalfirst>
                       <object_comment/>
                       <text>.</text>
                       <attribute_comment/>
                       <text> (</text>
                       <object_name/>
                       <text>.</text>
                       <attribute_name/>
                       <text>)</text>
                    </equalfirst>
                    <then>
                        <add_to_list list_name="AllMessages" pos="">
                           <text>������</text>
                           <object_name/>
                           <text>.</text>
                           <attribute_name/>
                        </add_to_list>
                        <add_to_list list_name="NotExist_Messages" pos="">
                           <text>������</text>
                           <object_name/>
                           <text>.</text>
                           <attribute_name/>
                        </add_to_list>                       
                        <add_to_list list_name="Temp Create message for attributes" pos="">
                           <get_element_from_list list_name="Create message for attributes" pos=""/>
                        </add_to_list>
                        <delete_from_list list_name="Create message for attributes" pos=""/>
                    </then>
                 </change>
              </object_loop_attributes>
           </loop_objects>
        </do>
        <condition equal="false">
            <first/>
            <equalfirst>
                <count_elements_list list_name="Create message for attributes"/>
            </equalfirst>
        </condition>
     </while>
     <while>
        <do>
           <add_to_list list_name="Create message for attributes" pos="">
              <get_element_from_list list_name="Temp Create message for attributes" pos=""/>
           </add_to_list>
           <delete_from_list list_name="Temp Create message for attributes" pos=""/>
        </do>
        <condition equal="false">
            <first/>
            <equalfirst>
                <count_elements_list list_name="Temp Create message for attributes"/>
            </equalfirst>
        </condition>
     </while>


     <while>
        <do>
           <loop_objects>
              <object_loop_attributes>
                 <change>
                    <first>
                       <get_element_from_list list_name="Create subresults for attributes" pos=""/>
                    </first>
                    <equalfirst>
                       <object_comment/>
                       <text>.</text>
                       <attribute_comment/>
                       <text> (</text>
                       <object_name/>
                       <text>.</text>
                       <attribute_name/>
                       <text>)</text>
                    </equalfirst>
                    <then>
                        <add_to_list list_name="AllMessages" pos="">
                           <text>�����������������������</text>
                           <object_name/>
                           <text>.</text>
                           <attribute_name/>
                        </add_to_list>
                        <add_to_list list_name="NotExist_Messages" pos="">
                           <text>������</text>
                           <object_name/>
                           <text>.</text>
                           <attribute_name/>
                        </add_to_list>                       
                        <add_to_list list_name="Temp Create subresults for attributes" pos="">
                           <get_element_from_list list_name="Create subresults for attributes" pos=""/>
                        </add_to_list>
                        <delete_from_list list_name="Create subresults for attributes" pos=""/>
                    </then>
                 </change>
              </object_loop_attributes>
           </loop_objects>
        </do>
        <condition equal="false">
            <first/>
            <equalfirst>
                <count_elements_list list_name="Create subresults for attributes"/>
            </equalfirst>
        </condition>
     </while>
     <while>
        <do>
           <add_to_list list_name="Create subresults for attributes" pos="">
              <get_element_from_list list_name="Temp Create subresults for attributes" pos=""/>
           </add_to_list>
           <delete_from_list list_name="Temp Create subresults for attributes" pos=""/>
        </do>
        <condition equal="false">
            <first/>
            <equalfirst>
                <count_elements_list list_name="Temp Create subresults for attributes"/>
            </equalfirst>
        </condition>
     </while>
<comment>
  ����� �����.
  ���� ���� ������� ������ ������������(��� �������������)
  � �� ������������(��� �� �������������) ���������
</comment>


<comment>
   ���� ���� ������� ������ ������������(��� �������������)
   � ������ �� ������������ (��� �� �������������) ��������� � ������������
</comment>
    <add_to_list list_name="NotExist_Scenario" pos="">
      <text>MainScenario</text>
    </add_to_list>
    <add_to_list list_name="Exist_Scenario" pos="">
      <text>MainScenario</text>
    </add_to_list>
<comment>
   ����� �����.
   ���� ���� ������� ������ ������������(��� �������������)
   � ������ �� ������������ (��� �� �������������) ��������� � ������������
</comment>


<comment>
   ���� ���� ������� ������, � ������� ��������� ��� �������� ���
   ���������� ��������� ��� ������������
</comment>
          <add_to_list list_name="Can add to scenario" pos="">
             <text>execute...(by russian)</text>
          </add_to_list>
          <add_to_list list_name="Can add to scenario" pos="">
             <text>send...(by russian)</text>
          </add_to_list>
          <add_to_list list_name="Can add to scenario" pos="">
             <text>send... to...(by russian)</text>
          </add_to_list>
          <add_to_list list_name="Can add to scenario" pos="">
             <text>set... to...(by russian)</text>
          </add_to_list>
          <add_to_list list_name="Can add to scenario" pos="">
             <text>when eqv(...) goto...(by russian)</text>
          </add_to_list>
          <add_to_list list_name="Can add to scenario" pos="">
             <text>when not(eqv(...)) goto...(by russian)</text>
          </add_to_list>
          <add_to_list list_name="Can add to scenario" pos="">
             <text>stop(by russian)</text>
          </add_to_list>
          <add_to_list list_name="Can add to scenario" pos="">
             <text>metka:(by russian)</text>
          </add_to_list>
<comment>
   ����� �����.
   ���� ���� ������� ������, � ������� ��������� ��� �������� ���
   ���������� ��������� ��� ������������
</comment>


<comment>
   ���� ���� ������� ������, ������� ������������ ��� ������
   ������������ ����������� ������������ ����������� ������ (���������� ������
   � ������� ��������� ��� ��������� ��)
</comment>
     <add_to_list list_name="AnswerList" pos="">
       <text>Continue...</text>
     </add_to_list>
     <add_to_list list_name="AnswerList" pos="">
       <text>Finish...</text>
     </add_to_list>
<comment>
   ���� ���� ������� ������, ������� ������������ ��� ������
   ������������ ����������� ������������ ����������� ������ (���������� ������
   � ������� ��������� ��� ��������� ��)
</comment>


<comment>
  ���� ���� ��������� ������ �� �������� ��������� � ������������
</comment>
    <while>
       <do>
          <change>
             <first>
                <text>MainScenario</text>
             </first>
             <equalfirst>
                <get_element_from_list list_name="NotExist_Scenario" pos=""/>
             </equalfirst>
             <then>
                <text>scenario </text>
             </then>
             <else>
                <text>subscenario </text>
             </else>
          </change>
          <get_element_from_list list_name="NotExist_Scenario" pos="1"/>
          <text>;
</text>
<comment>
  ���� ���������...
  ������� ������ �����, ������������ � ������ ��������...
</comment>
             <while>
               <do>
                  <delete_from_list list_name="ExistLabels" pos=""/>
               </do>
               <condition equal="false">
                  <first/>
                  <equalfirst>
                      <count_elements_list list_name="ExistLabels"/>
                  </equalfirst>
               </condition>
             </while>
             <while>
               <do>
                  <delete_from_list list_name="NotExistLabels" pos=""/>
               </do>
               <condition equal="false">
                  <first/>
                  <equalfirst>
                      <count_elements_list list_name="NotExistLabels"/>
                  </equalfirst>
               </condition>
             </while>
             <while>
               <do>
                  <delete_from_list list_name="AllLabels" pos=""/>
               </do>
               <condition equal="false">
                  <first/>
                  <equalfirst>
                      <count_elements_list list_name="AllLabels"/>
                  </equalfirst>
               </condition>
             </while>
<comment>
  ����� �����.
  ���� ���������...
  ������� ������ �����, ������������ � ������ ��������...
</comment>
          <while>
           <do>

            <dialog_message caption="�������� ��������� � ������������ �������..." type="" result_list_name="Answer">
               <text_box align="center" bold="">
                 <text>��������, ����������, �� ������ �������, ������� �� ������ �������� � �������� (�����������):</text>
               </text_box>
               <text_box align="center" bold="true">
                 <get_element_from_list list_name="NotExist_Scenario" pos="1"/>
               </text_box>
               <input_box caption="�������� ����������� ������� �� ������" type="listbox" list_name="Can add to scenario" default_number="1"/>
               <input_box caption="��� ����������� ������ � ������ ���������(������������) �������� Continue... ��� ����������� ������ � ��� �������� Finish..." type="enum_one" list_name="AnswerList" default_number="1"/>
            </dialog_message>
<comment>
  ���� ���� ���������� ������������������ �������� ��� ���������� 
  �������� �������� � ��������
</comment>
            <change>
              <first>
                 <get_element_from_list list_name="Answer" pos="2"/>
              </first>
              <equalfirst>
                 <text>Finish...</text>
              </equalfirst>
              <then/>
              <else>
                 <change>
                    <first>
                       <get_element_from_list list_name="Answer" pos="1"/>
                    </first>
                    <equalfirst>
                       <text>execute...(by russian)</text>
                    </equalfirst>
                    <then>
<comment>
  Scenarios... ���� ��� ������ EXECUTE...
</comment>
                       <while>
                          <do>
                             <delete_from_list list_name="AnswerExecute" pos=""/>
                          </do>
                          <condition equal="false">
                             <first/>
                             <equalfirst>
                                <count_elements_list list_name="AnswerExecute"/>
                             </equalfirst>
                          </condition>
                       </while>
                       <while>
                         <do>
                            <dialog_message caption="����� ������������� �����������..." type="" result_list_name="AnswerExecute">
                                <input_box caption="�������� �� ������ ��� ������� ����� �����������, ������� �� ������ ���������" type="listbox" list_name="AllScenario" default_number=""/>
                            </dialog_message>   
                         </do>
                         <condition equal="true">
                            <first/>
                            <equalfirst>
                               <get_element_from_list list_name="AnswerExecute" pos="1"/>
                            </equalfirst>
                         </condition>
                       </while>
                       <add_to_list list_name="AllScenario" pos="">
                          <get_element_from_list list_name="AnswerExecute" pos="1"/>
                       </add_to_list>
                       <add_to_list list_name="NotExist_Scenario" pos="">
                          <get_element_from_list list_name="AnswerExecute" pos="1"/>
                       </add_to_list>
                       <while>
                          <do>
                             <change>
                                <first>
                                   <get_element_from_list list_name="AnswerExecute" pos="1"/>
                                </first>
                                <equalfirst>
                                   <get_element_from_list list_name="Exist_Scenario" pos=""/>
                                </equalfirst>
                                <then>
                                   <delete_from_list list_name="NotExist_Scenario" pos=""/>
                                </then>
                             </change>
                             <add_to_list list_name="Temp Exist_Scenario" pos="">
                                <get_element_from_list list_name="Exist_Scenario" pos=""/>
                             </add_to_list>
                             <delete_from_list list_name="Exist_Scenario" pos=""/>
                          </do>
                          <condition equal="false">
                             <first/>
                             <equalfirst>
                               <count_elements_list list_name="Exist_Scenario"/>
                             </equalfirst>
                          </condition>
                       </while>
                       <while>
                          <do>
                             <add_to_list list_name="Exist_Scenario" pos="">
                                <get_element_from_list list_name="Temp Exist_Scenario" pos=""/>
                             </add_to_list>
                             <delete_from_list list_name="Temp Exist_Scenario" pos=""/>
                          </do>
                          <condition equal="false">
                             <first/>
                             <equalfirst>
                               <count_elements_list list_name="Temp Exist_Scenario"/>
                             </equalfirst>
                          </condition>
                       </while>
                       <text>  execute </text>
                       <get_element_from_list list_name="AnswerExecute" pos="1"/>
                       <text>;
</text>
<comment>                   
  ����� �����.
  Scenarios... ���� ��� ������ EXECUTE...
</comment>
                    </then>
<comment>
  Scenarios... ���� ��� ������ �� EXECUTE
</comment>
                    <else>
                      <change>
                         <first>
                           <get_element_from_list list_name="Answer" pos="1"/>
                         </first>
                         <equalfirst>
                           <text>send...(by russian)</text>
                         </equalfirst>
                         <then>
<comment>
   Scenarios... ���� ��� ������ SEND...
</comment>
                           <while>
                             <do>
                                <delete_from_list list_name="AnswerSend" pos=""/>
                             </do>
                             <condition equal="false">
                               <first/>
                               <equalfirst>
                                  <count_elements_list list_name="AnswerSend"/>
                               </equalfirst>
                            </condition>
                           </while>
                           <while>
                             <do>
                               <dialog_message caption="���������� ����������� ���������..." type="" result_list_name="AnswerSend">
                                  <input_box caption="�������� ���������� ��������� �� ������ ��� ������� �����" type="listbox" list_name="AllMessages" default_number=""/>
                               </dialog_message>   
                             </do>
                             <condition equal="true">
                               <first/>
                               <equalfirst>
                                  <get_element_from_list list_name="AnswerSend" pos="1"/>
                               </equalfirst>
                             </condition>
                           </while>
                           <add_to_list list_name="AllMessages" pos="">
                              <get_element_from_list list_name="AnswerSend" pos="1"/>
                           </add_to_list>
                           <add_to_list list_name="NotExist_Messages" pos="">
                              <get_element_from_list list_name="AnswerSend" pos="1"/>
                           </add_to_list>
                           <while>
                             <do>
                                <change>
                                   <first>
                                      <get_element_from_list list_name="AnswerSend" pos="1"/>
                                   </first>
                                   <equalfirst>
                                      <get_element_from_list list_name="Exist_Messages" pos=""/>
                                   </equalfirst>
                                   <then>
                                      <delete_from_list list_name="NotExist_Messages" pos=""/>
                                   </then>
                                </change>
                                <add_to_list list_name="Temp Exist_Messages" pos="">
                                   <get_element_from_list list_name="Exist_Messages" pos=""/>
                                </add_to_list>
                                <delete_from_list list_name="Exist_Messages" pos=""/>
                             </do>
                             <condition equal="false">
                               <first/>
                               <equalfirst>
                                 <count_elements_list list_name="Exist_Messages"/>
                               </equalfirst>
                             </condition>
                          </while>
                          <while>
                             <do>
                               <add_to_list list_name="Exist_Messages" pos="">
                                  <get_element_from_list list_name="Temp Exist_Messages" pos=""/>
                               </add_to_list>
                               <delete_from_list list_name="Temp Exist_Messages" pos=""/>
                             </do>
                             <condition equal="false">
                                <first/>
                                <equalfirst>
                                  <count_elements_list list_name="Temp Exist_Messages"/>
                                </equalfirst>
                             </condition>
                          </while>
                          <text>  send </text>
                          <get_element_from_list list_name="AnswerSend" pos="1"/>
                          <text>;
</text>
<comment>
  ����� �����
  Scenarios... ���� ��� ������ SEND...
</comment>
                         </then>
                         <else>
<comment>
   Scenarios... ���� ��� ������ �� SEND...
</comment>
                           <change>
                             <first>
                               <get_element_from_list list_name="Answer" pos="1"/>
                             </first>
                             <equalfirst>
                               <text>send... to...(by russian)</text>
                             </equalfirst>
                             <then>
<comment>
   Scenarios... ���� ��� ������ SEND... TO...
</comment>
                               <while>
                                 <do>
                                    <delete_from_list list_name="AnswerSendTo" pos=""/>
                                 </do>
                                 <condition equal="false">
                                   <first/>
                                   <equalfirst>
                                     <count_elements_list list_name="AnswerSendTo"/>
                                   </equalfirst>
                                 </condition>
                               </while>
                               <while>
                                 <do>
                                   <dialog_message caption="���������� ����������� ��������� ����-����..." type="" result_list_name="AnswerSendTo">
                                      <input_box caption="�������� ���������� ��������� �� ������ ��� ������� �����" type="listbox" list_name="AllMessages" default_number=""/>
                                      <input_box caption="�������� �� ������ ���������-���������� ��������� ��� ������� �����" type="listbox" list_name="AllComponents" default_number=""/>
                                   </dialog_message>   
                                 </do>
                                 <condition equal="false">
                                   <first>
                                      <text>11</text>
                                   </first>
                                   <equalfirst>
                                      <change>
                                         <first>
                                           <get_element_from_list list_name="AnswerSendTo" pos="1"/>
                                         </first>
                                         <equalfirst/>
                                         <then/>
                                         <else>
                                             <text>1</text>
                                         </else>
                                      </change>
                                      <change>
                                         <first>
                                           <get_element_from_list list_name="AnswerSendTo" pos="2"/>
                                         </first>
                                         <equalfirst/>
                                         <then/>
                                         <else>
                                             <text>1</text>
                                         </else>
                                      </change>
                                   </equalfirst>
                                 </condition>
                               </while>
                               <add_to_list list_name="AllComponents" pos="">
                                   <get_element_from_list list_name="AnswerSendTo" pos="2"/>
                               </add_to_list>
                               <text>  send '</text>
                               <get_element_from_list list_name="AnswerSendTo" pos="1"/>
                               <text>' to </text>
                               <get_element_from_list list_name="AnswerSendTo" pos="2"/>
                               <text>;
</text>
    
<comment>
   ����� �����.
   Scenarios...���� ��� ������ SEND... TO...
</comment>
                             </then>
                             <else>
<comment>
  Scenarios... ���� ��� ������ �� SEND... TO...
</comment>
                                <change>
                                   <first>
                                      <get_element_from_list list_name="Answer" pos="1"/>
                                   </first>
                                   <equalfirst>
                                      <text>set... to...(by russian)</text>
                                   </equalfirst>
                                   <then>
<comment>
   Scenarios... ���� ��� ������ SET... TO...
</comment>
                                     <while>
                                       <do>
                                          <delete_from_list list_name="AnswerSetTo" pos=""/>
                                       </do>
                                       <condition equal="false">
                                          <first/>
                                          <equalfirst>
                                             <count_elements_list list_name="AnswerSetTo"/>
                                          </equalfirst>
                                       </condition>
                                     </while>
                                     <while>
                                       <do>
                                         <dialog_message caption="����������� ��������/����������..." type="" result_list_name="AnswerSetTo">
                                            <input_box caption="�������� �� ������ �������/���������� ��� ������� �����" type="listbox" list_name="AllVariables" default_number=""/>
                                            <input_box caption="�������� �� ������ �������� ��� ������� �����" type="string" list_name="" default_number=""/>
                                         </dialog_message>   
                                       </do>
                                       <condition equal="true">
                                          <first/>
                                          <equalfirst>
                                              <get_element_from_list list_name="AnswerSetTo" pos="1"/>
                                          </equalfirst>
                                        </condition>
                                     </while>
                                     <add_to_list list_name="AllVariables" pos="">
                                        <get_element_from_list list_name="AnswerSetTo" pos="1"/>
                                     </add_to_list>
                                     <text>  set #</text>
                                     <get_element_from_list list_name="AnswerSetTo" pos="1"/>
                                     <text># to '</text>
                                     <get_element_from_list list_name="AnswerSetTo" pos="2"/>
                                     <text>';
</text>
    
<comment>
   ����� �����.
   Scenarios... ���� ��� ������ SET... TO...
</comment>
                                   </then>
                                   <else>
<comment>
  Scenarios... ���� ��� ������ �� SET... TO...
</comment>
                                      <change>
                                        <first>
                                           <get_element_from_list list_name="Answer" pos="1"/>
                                        </first>
                                        <equalfirst>
                                           <text>when eqv(...) goto...(by russian)</text>
                                        </equalfirst>
                                        <then>
                                           <add_to_list list_name="TempList" pos="">
                                              <text>OK</text>
                                           </add_to_list>
                                        </then>
                                        <else>
                                           <change>
                                             <first>
                                                 <get_element_from_list list_name="Answer" pos="1"/>
                                             </first>
                                             <equalfirst>
                                                <text>when not(eqv(...)) goto...(by russian)</text>
                                              </equalfirst>
                                              <then>
                                                 <add_to_list list_name="TempList" pos="">
                                                    <text>OK</text>
                                                 </add_to_list>
                                              </then>
                                              <else>
                                                 <add_to_list list_name="TempList" pos="">
                                                    <text>NOT OK</text>
                                                 </add_to_list>
                                              </else>
                                           </change>
                                        </else>
                                      </change>
                                      <change>
                                        <first>
                                           <get_element_from_list list_name="TempList" pos=""/>
                                        </first>
                                        <equalfirst>
                                           <text>OK</text>
                                        </equalfirst>
                                        <then>
<comment>
   Scenarios... ���� ���� ������� WHEN EQV(...) ��� WHEN NOT(EQV(...))
</comment>
                                          <delete_from_list list_name="TempList" pos=""/>
                                          <while>
                                            <do>
                                              <delete_from_list list_name="AnswerWhenEqv" pos=""/>
                                            </do>
                                            <condition equal="false">
                                              <first/>
                                              <equalfirst>
                                                 <count_elements_list list_name="AnswerWhenEqv"/>
                                              </equalfirst>
                                            </condition>
                                          </while>
                                          <while>
                                            <do>
                                              <dialog_message caption="���������� ��������� ���������..." type="" result_list_name="AnswerWhenEqv">
                                                 <input_box caption="�������� �� ������ ������(��) �������/���������� ��� ������� �����" type="listbox" list_name="AllVariables" default_number=""/>
                                                 <input_box caption="��� ������� ������ ��������� ���������" type="string" list_name="" default_number=""/>
                                                 <input_box caption="�������� �� ������ ������(��) �������/���������� ��� ������� �����" type="listbox" list_name="AllVariables" default_number=""/>
                                                 <input_box caption="��� ������� ������ ��������� ���������" type="string" list_name="" default_number=""/>
                                                 <input_box caption="�������� �� ������ ����� �������� ��� ���������������� ��� ������� �����" type="listbox" list_name="AllLabels" default_number=""/>
                                              </dialog_message>   
                                            </do>
                                            <condition equal="false">
                                              <first>
                                                <text>123</text>
                                              </first>
                                              <equalfirst>
                                                 <change>
                                                    <first>
                                                      <get_element_from_list list_name="AnswerWhenEqv" pos="1"/>
                                                    </first>
                                                    <equalfirst/>
                                                    <then/>
                                                    <else>
                                                      <text>1</text>
                                                    </else>
                                                 </change>
                                                 <change>
                                                    <first>
                                                      <get_element_from_list list_name="AnswerWhenEqv" pos="2"/>
                                                    </first>
                                                    <equalfirst/>
                                                    <then/>
                                                    <else>
                                                      <text>1</text>
                                                    </else>
                                                 </change>
                                                 <change>
                                                    <first>
                                                      <get_element_from_list list_name="AnswerWhenEqv" pos="3"/>
                                                    </first>
                                                    <equalfirst/>
                                                    <then/>
                                                    <else>
                                                      <text>2</text>
                                                    </else>
                                                 </change>
                                                 <change>
                                                    <first>
                                                      <get_element_from_list list_name="AnswerWhenEqv" pos="4"/>
                                                    </first>
                                                    <equalfirst/>
                                                    <then/>
                                                    <else>
                                                      <text>2</text>
                                                    </else>
                                                 </change>
                                                 <change>
                                                    <first>
                                                      <get_element_from_list list_name="AnswerWhenEqv" pos="5"/>
                                                    </first>
                                                    <equalfirst/>
                                                    <then/>
                                                    <else>
                                                      <text>3</text>
                                                    </else>
                                                 </change>
                                              </equalfirst>
                                            </condition>
                                         </while>
                                         <change>
                                            <first>
                                              <get_element_from_list list_name="Answer" pos="1"/>
                                            </first>
                                            <equalfirst>
                                               <text>when eqv(...) goto...(by russian)</text>
                                            </equalfirst>
                                            <then>
                                               <text>  when eqv(</text>
                                            </then>
                                            <else>
                                                     <text>  when not(eqv(</text>
                                            </else>
                                         </change>
                                         <change>
                                            <first>
                                                <get_element_from_list list_name="AnswerWhenEqv" pos="1"/>
                                            </first>
                                            <equalfirst/>
                                            <then>
                                               <text>'</text>
                                               <get_element_from_list list_name="AnswerWhenEqv" pos="2"/>
                                               <text>',</text>
                                            </then>
                                            <else>
                                               <add_to_list list_name="AllVariables" pos="">
                                                  <get_element_from_list list_name="AnswerWhenEqv" pos="1"/>
                                               </add_to_list>
                                               <text>#</text>
                                               <get_element_from_list list_name="AnswerWhenEqv" pos="1"/>
                                               <text>#,</text>
                                            </else>
                                         </change>
                                         <change>
                                            <first>
                                                <get_element_from_list list_name="AnswerWhenEqv" pos="3"/>
                                            </first>
                                            <equalfirst/>
                                            <then>
                                               <text>'</text>
                                               <get_element_from_list list_name="AnswerWhenEqv" pos="4"/>
                                               <text>')</text>
                                            </then>
                                            <else>
                                               <add_to_list list_name="AllVariables" pos="">
                                                  <get_element_from_list list_name="AnswerWhenEqv" pos="3"/>
                                               </add_to_list>
                                               <text>#</text>
                                               <get_element_from_list list_name="AnswerWhenEqv" pos="3"/>
                                               <text>#)</text>
                                            </else>
                                         </change>
                                         <change>
                                            <first>
                                              <get_element_from_list list_name="Answer" pos="1"/>
                                            </first>
                                            <equalfirst>
                                               <text>when eqv(...) goto...(by russian)</text>
                                            </equalfirst>
                                            <then>
                                                     <text> goto </text>
                                            </then>
                                            <else>
                                                     <text>) goto </text>
                                            </else>
                                         </change>
                                         <add_to_list list_name="AllLabels" pos="">
                                            <get_element_from_list list_name="AnswerWhenEqv" pos="5"/>
                                         </add_to_list>
                                         <add_to_list list_name="NotExistLabels" pos="">
                                            <get_element_from_list list_name="AnswerWhenEqv" pos="5"/>
                                         </add_to_list>
                                         <while>
                                           <do>
                                              <change>
                                                 <first>
                                                    <get_element_from_list list_name="AnswerWhenEqv" pos="5"/>
                                                 </first>
                                                 <equalfirst>
                                                     <get_element_from_list list_name="ExistLabels" pos=""/>
                                                 </equalfirst>
                                                 <then>
                                                     <delete_from_list list_name="NotExistLabels" pos=""/>
                                                 </then>
                                              </change>
                                              <add_to_list list_name="Temp ExistLabels" pos="">
                                                 <get_element_from_list list_name="ExistLabels" pos=""/>
                                              </add_to_list>
                                              <delete_from_list list_name="ExistLabels" pos=""/>
                                           </do>
                                           <condition equal="false">
                                              <first/>
                                              <equalfirst>
                                                 <count_elements_list list_name="ExistLabels"/>
                                              </equalfirst>
                                           </condition>
                                         </while>
                                         <while>
                                            <do>
                                               <add_to_list list_name="ExistLabels" pos="">
                                                   <get_element_from_list list_name="Temp ExistLabels" pos=""/>
                                               </add_to_list>
                                               <delete_from_list list_name="Temp ExistLabels" pos=""/>
                                            </do>
                                            <condition equal="false">
                                               <first/>
                                               <equalfirst>
                                                   <count_elements_list list_name="Temp ExistLabels"/>
                                               </equalfirst>
                                            </condition>
                                         </while>
                                         <get_element_from_list list_name="AnswerWhenEqv" pos="5"/>
                                         <text>;
</text>
<comment>
   ����� �����.
   Scenarios... ���� ��� ������ WHEN EQV(...)
</comment>
                                       </then>
                                       <else>
<comment>
   Scenarios... ���� ��� ������ �� WHEN EQV(...)
</comment>
                                         <change>
                                            <first>
                                               <get_element_from_list list_name="Answer" pos="1"/>
                                            </first>
                                            <equalfirst>
                                               <text>stop(by russian)</text>
                                            </equalfirst>
                                            <then>
<comment>
   Scenarios... ���� ��� ������ STOP
</comment>                                    
                                            <text>  stop;
</text>
<comment>
  ����� �����.
  Scenarios... ���� ��� ������ STOP
</comment>                                    
                                            </then>
                                            <else>
<comment>
   Scenarios... ���� ��� ������ �� STOP
</comment>                                    
                                            </else>
                                          </change>
                                       </else>
                                     </change>
                                   </else>
                                </change>
                             </else>
                           </change>
                         </else>
                      </change>
                    </else>
                 </change>
              </else>
           </change>
<comment>
  ����� �����.
  ���� ���� ���������� ������������������ �������� ��� ���������� 
  �������� �������� � ��������
</comment>
           </do>
           <condition equal="false">
              <first>
                 <get_element_from_list list_name="Answer" pos="2"/>
              </first>
              <equalfirst>
                 <text>Finish...</text>
              </equalfirst>
           </condition>
          </while>
          <while>
             <do>
                <text>
</text>
                <get_element_from_list list_name="NotExistLabels" pos=""/>
                <text>:
</text>
                <delete_from_list list_name="NotExistLabels" pos=""/>
             </do>
             <condition equal="false">
                <first/>
                <equalfirst>
                    <count_elements_list list_name="NotExistLabels"/>
                </equalfirst>
             </condition>
          </while>
          <text>end;

</text>
<comment>
  ��������... 
  ���� ���� ������� ������ ����� �������������� � ���...
</comment>
             <while>
               <do>
                  <delete_from_list list_name="ExistLabels" pos=""/>
               </do>
               <condition equal="false">
                  <first/>
                  <equalfirst>
                      <count_elements_list list_name="ExistLabels"/>
                  </equalfirst>
               </condition>
             </while>
             <while>
               <do>
                  <delete_from_list list_name="NotExistLabels" pos=""/>
               </do>
               <condition equal="false">
                  <first/>
                  <equalfirst>
                      <count_elements_list list_name="NotExistLabels"/>
                  </equalfirst>
               </condition>
             </while>
             <while>
               <do>
                  <delete_from_list list_name="AllLabels" pos=""/>
               </do>
               <condition equal="false">
                  <first/>
                  <equalfirst>
                      <count_elements_list list_name="AllLabels"/>
                  </equalfirst>
               </condition>
             </while>
<comment>
  ����� �����.
  ��������... 
  ���� ���� ������� ������ ����� �������������� � ���...
</comment>
          <delete_from_list list_name="NotExist_Scenario" pos="1"/>
          <delete_from_list list_name="Answer" pos="1"/>
          <delete_from_list list_name="Answer" pos="2"/>
      </do>
       <condition equal="false">
          <first/>
          <equalfirst>
             <count_elements_list list_name="NotExist_Scenario"/>
          </equalfirst>
       </condition>
    </while>
<comment>
  ����� �����.
  ���� ���� ��������� ������ �� �������� ��������� � ������������
</comment>


<comment>
  ���� ���� ��������� ������ � ������������� ��� ��������
  ���������� ����������� ��� ����������, ������� �� ����� ���� �������� ���������
</comment>
     <add_to_list list_name="Type of dialog" pos="">
       <text>as Question</text>
     </add_to_list>
     <add_to_list list_name="Type of dialog" pos="">
       <text>as Attention</text>
     </add_to_list>
     <add_to_list list_name="Type of dialog" pos="">
       <text>as Information</text>
     </add_to_list>
     <add_to_list list_name="Type of dialog" pos=""/>
     <while>
        <do>
           <loop_objects>
              <object_loop_attributes>
                  <change>
                     <first>
                        <get_element_from_list list_name="Create message for attributes" pos=""/>
                     </first>
                     <equalfirst>
                         <object_comment/>
                         <text>.</text>
                         <attribute_comment/>
                         <text> (</text>
                         <object_name/>
                         <text>.</text>
                         <attribute_name/>
                         <text>)</text>
                     </equalfirst>
                     <then>
                         <while>
                             <do>
                                <delete_from_list list_name="Pogreshnost" pos=""/>
                             </do>
                             <condition equal="false">
                                <first/>
                                <equalfirst>
                                   <count_elements_list list_name="Pogreshnost"/>
                                </equalfirst>
                             </condition>
                         </while>
                         <attribute_type>
                            <if_type_symbolic>
                               <then>
                                  <add_to_list list_name="Pogreshnost" pos="">
                                     <text>No</text>
                                  </add_to_list>
                                  <add_to_list list_name="Pogreshnost" pos="">
                                     <text>Uverrennost</text>
                                  </add_to_list>
                                  <add_to_list list_name="Pogreshnost" pos="">
                                     <text>Double Uverrennost</text>
                                  </add_to_list>
                               </then>
                            </if_type_symbolic>
                            <if_type_numeric>
                               <then>
                                  <add_to_list list_name="Pogreshnost" pos="">
                                     <text>No</text>
                                  </add_to_list>
                                  <add_to_list list_name="Pogreshnost" pos="">
                                     <text>Pogreshnost</text>
                                  </add_to_list>
                               </then>
                            </if_type_numeric>
                         </attribute_type>
                 
                         <dialog_message caption="�������� ���������� ����������� ��� ������������ ���������..." type="question" result_list_name="Dialog create message">
                            <text_box align="left" bold="">
                               <text>��������� ��������� ��� ��������: </text>
                               <attribute_comment/> 
                               <text> �������: </text>
                               <object_comment/>
                               <text> (</text>
                               <object_name/>
                               <text>.</text>
                               <attribute_name/>
                               <text>).</text>
                            </text_box>
                            <input_box caption="������� ��������� ���� ����������� ����������" type="string" list_name="" default_number="">
                               <default_string>
                                  <text>��������� �������� ��������: </text>
                                  <attribute_comment/>
                                  <text> � �������: </text> 
                                  <object_comment/>
                                  <text>...</text>
                               </default_string>
                            </input_box>
                            <input_box caption="������� ���������� ������" type="string" list_name="" default_number="">
                               <default_string>
                                  <text>������ �������� ��������: </text>
                                  <attribute_comment/>
                                  <text> � �������: </text> 
                                  <object_comment/>
                                  <text>?</text>
                               </default_string>
                            </input_box>
                            <input_box caption="���������� ��� ����������� ����(����� �� ������)?" type="listbox" list_name="Type of dialog" default_number="1"/>
                            <input_box caption="���������� ��� ����������/���������� ��� ������?" type="listbox" list_name="Pogreshnost" default_number="1"/>
                            <input_box caption="���������� ���� � �����-�������, ����������� � ������� ����������(����� �� ���������)?" type="string" list_name="" default_number=""/>
                            <input_box caption="���������� �������� ������� ��������� � ����� ��������� �������" type="string" list_name="" default_number="">
                               <default_string>
                                  <text>������</text>
                                  <object_name/>
                                  <text>_</text> 
                                  <attribute_name/>
                               </default_string>
                            </input_box>
                         </dialog_message>
                         <text>message </text>
                         <get_element_from_list list_name="Dialog create message" pos=""/>
                         <text> to Asker about #</text>
                         <object_name/>
                         <text>.</text>
                         <attribute_name/>
                         <text>#;
</text>
                         <text>  line 'set Caption to $'</text>
                         <get_element_from_list list_name="Dialog create message" pos="1"/>
                         <text>$'';
</text>
                         <text>  line 'output [$'</text>
                         <get_element_from_list list_name="Dialog create message" pos="2"/>
                         <text>$'] </text>
                         <get_element_from_list list_name="Dialog create message" pos="3"/>
                         <text>';
</text>
                         <attribute_type>
                            <if_type_numeric>
                               <then>
                                  <text>  line 'input $'$' to #</text>
                                  <object_name/>
                                  <text>.</text>
                                  <attribute_name/>
                                  <text># as </text>
                                  <change>
                                     <first>
                                        <get_element_from_list list_name="Dialog create message" pos="4"/>
                                     </first>
                                     <equalfirst>
                                         <text>No</text>
                                     </equalfirst>
                                     <then/>
                                     <else>
                                        <text>Inexact</text>
                                     </else>
                                  </change>
                                  <text>Number';
</text>
                               </then>
                            </if_type_numeric>
                            <if_type_symbolic>
                               <then>
                                  <text>  line 'input $'$' to #</text>
                                  <object_name/>
                                  <text>.</text>
                                  <attribute_name/>
                                  <text># as </text>
                                  <change>
                                     <first>
                                        <get_element_from_list list_name="Dialog create message" pos="4"/>
                                     </first>
                                     <equalfirst>
                                         <text>Uverrennost</text>
                                     </equalfirst>
                                     <then>
                                        <text>Indef</text>
                                     </then>
                                     <else>
                                        <change>
                                           <first>
                                              <get_element_from_list list_name="Dialog create message" pos="4"/>
                                           </first>
                                           <equalfirst>
                                               <text>Double Uverrennost</text>
                                           </equalfirst>
                                           <then>
                                              <text>Subdef</text>
                                           </then>
                                         </change>
                                     </else>
                                  </change>
                                  <text>Variant from [</text>
                                  <type_loop_values>
                                    <text>$'</text>
                                    <type_value/>
                                    <text>$'</text>
                                    <if_type_value_last>
                                       <then/>
                                       <else>
                                          <text>,</text>
                                       </else>
                                    </if_type_value_last>
                                  </type_loop_values>
                                  <text>]';
</text>
                              </then>
                            </if_type_symbolic>
                         </attribute_type>
                         <text>  line 'activate'
</text>
                         <text>end;

</text>
                     </then>
                  </change>
              </object_loop_attributes>
           </loop_objects>
           <delete_from_list list_name="Create message for attributes" pos=""/>
        </do>
        <condition equal="false">
           <first/>
           <equalfirst>
              <count_elements_list list_name="Create message for attributes"/>
           </equalfirst>
        </condition>
     </while>
<comment>
  ���� ���� ��������� ������ � ������������� ��� ��������
  ���������� ����������� ��� ����������, ������� �� ����� ���� �������� ���������
</comment>
  </part>
</format_file>