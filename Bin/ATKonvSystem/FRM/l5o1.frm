<format_file>
<part file_ext="prl" encoding="">
<comment> Это форматный файл для преобразования поля знаний в БЗ инструментального комплекса Level5 Object </comment>
<comment> Автор: Ивлиев Р.С. </comment>
<comment> 30.10.2002 </comment>
<comment>Ver 1.0.3</comment>
<comment>- ------------------------------------------------------------------------------------</comment>
<comment> Здесь описываются директивы препроцессора Level5 Object</comment>
<text>$VERSION35
</text>
<text>$LOCATIONS ARE PIXELS 
</text>
<comment>- ------------------------------------------------------------------------------------</comment>
<comment>Проходимся сначала по объектам, т.е. получаем классы</comment>
<loop_objects>   
<text>CLASS Class_</text>
<object_name/>
<text>
</text>
<object_loop_attributes>
<text>WITH _</text><attribute_name/>
<loop_types>
<change>
<first><attribute_type><type_name/></attribute_type></first>
<equalfirst><type_name/></equalfirst>
<then>
<comment>The Begin Of Description of Symbolic Type</comment>
       <if_type_symbolic>
       <then>
       <text>  MULTICOMPOUND
       </text>
     <type_loop_values>
     <if_type_value_last>
    <then>
    <text>_</text><type_value/>
    <text>
    </text>
    </then>
    <else>
    <text>_</text><type_value/><text>,
 </text>
</else>
</if_type_value_last>
</type_loop_values>
<text> !  </text><type_comment/>
<text>
</text>
<text>WHEN NEEDED
</text>
<text>BEGIN</text>
<text>
</text>
<text>ASK  need_Class_</text><object_name/>
<text>_</text><attribute_name/>
<text>
</text>	
<text>END</text>	
<text>
</text>
<text>SEARCH ORDER CONTEXT RULES WHEN NEEDED DEFAULT 
</text>
</then>
<else>
</else>
</if_type_symbolic> 
<comment>The End Of Description Of Symbolic Type</comment>
<comment>The Begin Of Description Of Numeric Type</comment>
<if_type_numeric>
<then>
<text> NUMERIC
</text>
<text>! </text><type_comment/>
<text>
</text>
<text> WHEN NEEDED 
</text>
<text>BEGIN</text>
<text>
</text>
<text>ASK  need_Class_</text><object_name/>
<text>_</text><attribute_name/>
<text>
</text>	
<text>END</text>	
<text>
</text>

<text>SEARCH ORDER CONTEXT RULES WHEN NEEDED DEFAULT 
</text>
</then>
<else>
</else>
</if_type_numeric>
 <comment>The End Of Description Of Numeric Type</comment>
<comment>The Begin Of Description Of Fuzzy Type</comment>
<comment>Вот здесь я не знаю точно пока что делать...можно его просто проигнорировать, а можно .....вообщем пока так просто влепил стринг, а там посмотрим</comment>
<if_type_fuzzy>
<then>
<message type="warning"> Внимание! Обнаружен тип НЕЧЁТКИЙ. Вероятна потеря данных</message>
<text> STRING
</text>
<text>! Это был нечёткий тип</text>
</then>
<else>
</else>
</if_type_fuzzy>	
<comment>The End Of Description Of Fuzzy Type</comment>
</then>
<else>
</else>
</change> 
</loop_types>
<text>
</text>
</object_loop_attributes>
</loop_objects>
<text>
</text>
<comment>Создаём экземпляры классов</comment>
<loop_objects>
<text>INSTANCE Class_</text><object_name/><text> 1</text>  <text> ISA Class_</text><object_name/>
<text>
</text>
</loop_objects>
<text>
</text>
<comment>Конец создания экземпляров классов</comment>
<comment>------------------------------------------------------------------------</comment>

<comment>Здесь конец попыток визуализировать все экземпляры</comment>
<loop_relations>
<message type="warning">Обнаружены ОТНОШЕНИЯ! Вероятна потеря данных</message>
</loop_relations>
<comment> чисто теоретически мы должны получить конструкцию похожую не правду...</comment>
<text>
</text>

<comment> Здесь описываются правила</comment>
<loop_rules>
<text>RULE  </text>
<rule_name/>
<text>
</text>
<text>IF </text>
<rule_make_logic_value and="AND" or="OR"  xor=" " not="NOT" and_var="infix" or_var="infix" xor_var="infix" comma="" obracket="(" cbracket=")">
<separator></separator>
<comment>-------------------------------------------------------</comment>
<if_numeric_assertion>
<then>
<nassertion_attribute>
<text>_</text><attribute_name/>
</nassertion_attribute>
<text> OF Class_</text>
<nassertion_object>
<object_name/>
</nassertion_object>
<text> 1 </text>
<nassertion_operation/>
<nassertion_make_expression add="+" sub="-" mul="*" div="/" power="^" neg="-" add_var="infix" sub_var="infix" mul_var="infix" power_var="infix" div_var="infix" comma="," obracket="(" cbracket=")">
<if_number>
<then>
<number_value/>
</then>
</if_number>
<if_numeric_value>
<then>
<nv_attribute>
<text>_</text><attribute_name/>
</nv_attribute>
<text> OF Class_</text>
<nv_object>
<object_name/>
</nv_object>
<text> 1</text>
</then>
</if_numeric_value>
</nassertion_make_expression>
<assertion_loop_properties>
<if_prop_interval>
<then>
<text></text>
<comment>pi_belief_value/</comment>
</then>
</if_prop_interval>
</assertion_loop_properties>
<assertion_loop_properties>
<if_prop_accuracy>
<then>
<text></text>
</then>
</if_prop_accuracy>
</assertion_loop_properties>
</then>
 </if_numeric_assertion>

<comment>------------------------------------------</comment>
<comment>Если символьная конструкция</comment>
<if_symbolic_assertion>
<then>
<sassertion_attribute>
<text>_</text><attribute_name/>
</sassertion_attribute>
<text> OF Class_</text>
<sassertion_object>
<object_name/>
 </sassertion_object>		
<text> 1 IS _</text>
<sassertion_value/>
<text></text>
<assertion_loop_properties>
<if_prop_interval>
<then>
<text></text>
<comment>pi_belief_value/</comment>
</then>
</if_prop_interval>
</assertion_loop_properties>
</then>
 </if_symbolic_assertion>

<comment>А ещё лажа полная::::: ваще нету отношений в L5O </comment>
</rule_make_logic_value>
<comment> -----------------END IF-----------------------------</comment>
<text>
</text>
<text>
THEN </text>
<rule_loop_asser_then>
<if_numeric_assertion>
<then>
<nassertion_attribute>
<text>_</text><attribute_name/>
</nassertion_attribute>
<text> OF Class_</text>
<nassertion_object>
<object_name/>
</nassertion_object>
<text> 1  := </text>
<comment> здесь кроме этого ничего не получится nassertion_operation/>
</comment>
<nassertion_make_expression add="+" sub="-" mul="*" div="/" power="^" neg="-" add_var="infix" sub_var="infix" mul_var="infix" power_var="infix" div_var="infix" comma="," obracket="(" cbracket=")">
<if_number>
<then>
<number_value/>
</then>
</if_number>
<if_numeric_value>
<then>
<nv_attribute>
<text>_</text><attribute_name/>
</nv_attribute>
<text> OF Class_</text>
<nv_object>
<object_name/>
</nv_object>
<text> 1 </text>
		</then>
</if_numeric_value>
</nassertion_make_expression>
<text></text>
<assertion_loop_properties>
<if_prop_interval>
<then>
 	<text> CF </text>
<pi_belief_value/>
</then>
</if_prop_interval>
</assertion_loop_properties>
<text>
</text>
</then>
</if_numeric_assertion>

 <if_symbolic_assertion>
 	<then>
<sassertion_attribute>
<text>_</text><attribute_name/>
</sassertion_attribute>
<text> OF Class_</text>
<sassertion_object>
<object_name/>
</sassertion_object>
<text> 1  IS _</text>
<sassertion_value/>
<text></text>
<assertion_loop_properties>
<if_prop_interval>
<then>
<text> CF </text>
<pi_belief_value/>
</then>
</if_prop_interval>
</assertion_loop_properties>
<text>
</text>
</then>
</if_symbolic_assertion>

</rule_loop_asser_then>
<comment>--------------------END OF THEN ---------------------------------</comment>
<if_rule_has_else>
<then>
<text>
</text>
 <text>ELSE </text>
<rule_loop_asser_else>
<if_numeric_assertion>
<then>
<nassertion_attribute>
<text>_</text><attribute_name/>
</nassertion_attribute>
<text> OF Class_</text>
<nassertion_object>
<object_name/>
</nassertion_object>
<text> 1 </text>
<comment> Тоже что и в THEN nassertion_operation/>
</comment>
<text>:= </text>
<nassertion_make_expression add="+" sub="-" mul="*" div="/" power="^" neg="-" add_var="infix" sub_var="infix" mul_var="infix" power_var="infix" div_var="infix" comma="," obracket="(" cbracket=")">
<if_number>
<then>
<number_value/>
</then>
</if_number>
<if_numeric_value>
<then>
<nv_attribute>
<text>_</text><attribute_name/>
</nv_attribute>
<text> OF Class_</text>
<nv_object>
<object_name/>
</nv_object>
<text> 1 </text>
</then>
</if_numeric_value>
</nassertion_make_expression>
<text></text>
<assertion_loop_properties>
<if_prop_interval>
<then>
<text> CF </text>
<pi_belief_value/>
</then>
</if_prop_interval>
</assertion_loop_properties>
<text>
</text>
</then>
</if_numeric_assertion>
<if_symbolic_assertion>
<then>
<sassertion_attribute>
<text>_</text><attribute_name/>
</sassertion_attribute>
<text> OF Class_</text>
<sassertion_object>
<object_name/>
</sassertion_object>
<text> 1 IS _</text>
<sassertion_value/>
<text></text>
<assertion_loop_properties>
<if_prop_interval>
<then>
<text> CF </text>
<pi_belief_value/>
</then>
</if_prop_interval>
</assertion_loop_properties>
<text>
</text>
</then>
</if_symbolic_assertion>

</rule_loop_asser_else>
</then>
</if_rule_has_else>

<text>! </text>
<rule_comment/>
<text> 
</text>
</loop_rules>
<loop_linguistics>
<message type="warning">Внимание! Обнаружено описание СЛОВАРЯ(ей)! Данные будут утеряны</message> 

</loop_linguistics>
<text>
</text>
<text>END</text>
</part>
</format_file>
