<format_file>
 <part file_ext="kb" encoding=""> 
   <options>
     <change_values all="" types_symbolic="true" assertions_symbolic="">
       <change_substr>
         <remove_substr>
           <text>(</text>
         </remove_substr>
         <insert_substr>
           <text>_</text>
         </insert_substr>
       </change_substr>
       <change_substr>
         <remove_substr>
           <text>)</text>
         </remove_substr>
         <insert_substr>
           <text>_</text>
         </insert_substr>
       </change_substr>
       <change_substr>
         <remove_substr>
           <text>,</text>
         </remove_substr>
         <insert_substr>
           <text>_</text>
         </insert_substr>
       </change_substr>
       <change_substr>
         <remove_substr>
           <text>;</text>
         </remove_substr>
         <insert_substr>
           <text>_</text>
         </insert_substr>
       </change_substr>
       <change_substr>
         <remove_substr>
           <text> </text>
         </remove_substr>
         <insert_substr>
           <text>_</text>
         </insert_substr>
       </change_substr>
       <change_substr>
         <remove_substr>
           <text>.</text>
         </remove_substr>
         <insert_substr>
           <text>_</text>
         </insert_substr>
       </change_substr>
       <change_substr>
         <remove_substr>
           <text>-</text>
         </remove_substr>
         <insert_substr>
           <text>_</text>
         </insert_substr>
       </change_substr>
      </change_values>
   </options>
   <loop_objects>
     <object_loop_attributes>
       <text>

  initially in order
  create a symbolic-variable V and
  transfer V to LOADED_ATTRIBUTES and
  change the name of V to sybol ("VO</text>
       <object_number/>
       <text>A</text>
       <attribute_number/>
       <text>") and
  move V by (100,</text>
       <object_number/>
       <attribute_number/>
       <text>)
  { and make V permanent }</text>
       <attribute_type>
         <type_loop_values>
           <text>

  initially in order
  create a symbolic-variable V and
  transfer V to LOADED_ATTRIBUTES and
  change the name of V to sybol ("ZO</text>
           <object_number/>
           <text>A</text>
           <attribute_number/>
           <text>C</text>
           <type_value_number/>
           <text>") and
  move V by (200,</text>
           <object_number/>
           <attribute_number/>
           <type_value_number/>
           <text>)
  { and make V permanent } and
  conclude that V = symbol(</text>
           <type_value/>
           <text>)</text>
         </type_loop_values>
       </attribute_type>
     </object_loop_attributes>
   </loop_objects>
   <loop_rules>
     <text>

  initially in order create a rule {</text>
     <rule_number/>
     <text>} R
  and change the text of R to "
  if </text>
     <rule_make_logic_value and="AND" or="OR" xor="XOR" not="NOT" and_var="infix" or_var="infix" xor_var="infix" comma="," obracket="(" cbracket=")">
       <separator>
</separator>
        <if_symbolic_assertion>
          <then>
            <text>VO</text>
            <sassertion_object>
              <sassertion_attribute> 
                <object_number/>
                <text>A</text>
                <attribute_number/>
                <text>=ZO</text>
                <object_number/>                
                <text>A</text>
                <attribute_number/>
                <text>C</text> 
                <attribute_type>
                   <type_value_number_equal>
                     <sassertion_value/>
                   </type_value_number_equal>
                </attribute_type>
              </sassertion_attribute>
            </sassertion_object>
          </then>
          <else/>
        </if_symbolic_assertion>
     </rule_make_logic_value>
     <text>
  then conclude that</text>
     <rule_loop_asser_then>
        <if_assertion_last type="symbolic">
          <then>
            <text>  VO</text>
            <sassertion_object>
              <sassertion_attribute> 
                <object_number/>
                <text>A</text>
                <attribute_number/>
                <text>=ZO</text>
                <object_number/>                
                <text>A</text>
                <attribute_number/>
                <text>C</text> 
                <attribute_type>
                   <type_value_number_equal>
                     <sassertion_value/>
                   </type_value_number_equal>
                </attribute_type>
              </sassertion_attribute>
            </sassertion_object>
          </then>
          <else>
            <text>  VO</text>
            <sassertion_object>
              <sassertion_attribute> 
                <object_number/>
                <text>A</text>
                <attribute_number/>
                <text>=ZO</text>
                <object_number/>                
                <text>A</text>
                <attribute_number/>
                <text>C</text> 
                <attribute_type>
                   <type_value_number_equal>
                     <sassertion_value/>
                   </type_value_number_equal>
                </attribute_type>
              </sassertion_attribute> 
            </sassertion_object>
            <text> AND
</text>
          </else>
        </if_assertion_last>
     </rule_loop_asser_then>
     <text> " and
  transfer R to LOADED_RULES
  { and make R permanent }</text>
    </loop_rules>
 </part>
</format_file>
