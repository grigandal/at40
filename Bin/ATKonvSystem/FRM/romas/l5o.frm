<format_file>
<comment> Это форматный файл для преобразования поля знаний в БЗ инструментального комплекса Level5 Object </comment>
<comment> Автор: Ивлиев Р.С. </comment>
<comment> 30.10.2002 </comment>
<comment>Ver 1.0.3</comment>
<comment>- ------------------------------------------------------------------------------------</comment>
<comment> Здесь описываются директивы препроцессора Level5 Object</comment>
<text>$VERSION35
</text>
<text>$LOCATIONS ARE PIXELS 
</text>
<comment>- ------------------------------------------------------------------------------------</comment>
<comment>Проходимся сначала по объектам, т.е. получаем классы</comment>
<loop_objects>   
<text>CLASS Class_</text>
<object_name/>
<text>
</text>
<object_loop_attributes>
<text>WITH _</text><attribute_name/>
<loop_types>
<change>
<first><attribute_type><type_name/></attribute_type></first>
<equalfirst><type_name/></equalfirst>
<then>
<comment>The Begin Of Description of Symbolic Type</comment>
       <if_type_symbolic>
       <then>
       <text>  MULTICOMPOUND
       </text>
     <type_loop_values>
     <if_type_value_last>
    <then>
    <text>_</text><type_value/>
    <text>
    </text>
    </then>
    <else>
    <text>_</text><type_value/><text>,
 </text>
</else>
</if_type_value_last>
</type_loop_values>
<text> !  </text><type_comment/>
<text>
</text>
<text>BEGIN
</text>
<text>ASK  need_Class_</text><object_name/>
<text>_</text><attribute_name/>
<text>
</text>	
<text>END
</text>	
<text>SEARCH ORDER CONTEXT RULES WHEN NEEDED DEFAULT 
</text>
</then>
<else>
</else>
</if_type_symbolic> 
<comment>The End Of Description Of Symbolic Type</comment>
<comment>The Begin Of Description Of Numeric Type</comment>
<if_type_numeric>
<then>
<text> NUMERIC
</text>
<text>! </text><type_comment/>
<text>
</text>
<text>BEGIN
</text>
<text>ASK  need_Class_</text><object_name/>
<text>_</text><attribute_name/>
<text>
</text>	
<text>END
</text>	
<text>SEARCH ORDER CONTEXT RULES WHEN NEEDED DEFAULT 
</text>
</then>
<else>
</else>
</if_type_numeric>
 <comment>The End Of Description Of Numeric Type</comment>
<comment>The Begin Of Description Of Fuzzy Type</comment>
<comment>Вот здесь я не знаю точно пока что делать...можно его просто проигнорировать, а можно .....вообщем пока так просто влепил стринг, а там посмотрим</comment>
<if_type_fuzzy>
<then>
<text> STRING
</text>
<text>! </text><type_comment/>
<text>
</text>
<text>BEGIN 
</text>
<text>ASK need_Class_</text><object_name/>
<text>_</text><attribute_name/>
<text>
</text>	
<text>END
</text>	
<text>SEARCH ORDER CONTEXT RULES WHEN NEEDED DEFAULT 
</text>
</then>
<else>
</else>
</if_type_fuzzy>	
<comment>The End Of Description Of Fuzzy Type</comment>
</then>
<else>
</else>
</change> 
</loop_types>
<text>
</text>
</object_loop_attributes>
</loop_objects>
<text>
</text>
<comment>Создаём экземпляры классов</comment>
<loop_objects>
<text>INSTANCE Ex_</text><object_name/><text> ISA Class_</text><object_name/>
<text>
</text>
</loop_objects>
<text>
</text>
<comment>Конец создания экземпляров классов</comment>
<comment>------------------------------------------------------------------------</comment>
<comment>Здесь визуализация окон и кнопок</comment>
<comment>Это типа кнопка перехода </comment>
<text>
</text>
<text>INSTANCE go_button ISA pushbutton
</text>
<text>WITH label := "Продолжить"
</text>
<text>WITH attribute attachment := continue display OF main window
</text>
<text>WITH location := 5,5,100,40
</text>
<comment>А это типа выводной экран</comment>
<text>
</text>
<text>INSTANCE output_display ISA display
</text>
<text>WITH wait := TRUE
</text>
<text>WITH delay changes := FALSE
</text>
<text>WITH menus [1 ] := UNDETERMINED
</text>
<text>
</text>
<comment>Конец визуализации окон и кнопок</comment>
<comment>------------------------------------------------------------------------</comment>
<comment>Здесь попробуем визуализировать все экземпляры</comment>
<loop_objects>   
<object_loop_attributes>
<loop_types>
<change>
<first><attribute_type><type_name/></attribute_type></first>
<equalfirst><type_name/></equalfirst>
<then>
<comment>The Begin Of Visualisation of Symbolic Type</comment>       <if_type_symbolic>
<then>
<text>
</text>
<text>INSTANCE ValueBox_</text><object_name/>
<text>_</text><attribute_name/><text> ISA valuebox
</text>
<text>WITH justify IS left
</text>
<text>WITH font := "System"
</text>
<text>WITH frame := TRUE
</text>
<text>WITH clipped := TRUE
</text>
<text>WITH attachment :=</text><attribute_name/><text> OF Ex_</text><object_name/>
<text>
</text>
<text>WITH location := 10,10,200,100
</text>
<text>
</text>
<text>INSTANCE TextBox_Class_</text><object_name/><text>_</text><attribute_name/> <text> ISA textbox</text>
<text>
</text>
<text>WITH justify IS left
</text>
<text>WITH font := "System"
</text>
<text> WITH text := "Значение атрибута </text><type_comment/><text> объекта </text><object_comment/> <text>"</text>
<text>
</text>
<text>WITH location := 0,0,110,10
</text>
<text>
</text>
<text>INSTANCE CheckGroup_Class_</text> <object_name/><text> ISA checkbox group</text>
<text>
</text>
<text>WITH frame := TRUE
</text>
<text> WITH group label := "Вы уверены в значении атрибута </text>  <type_comment/> 
<text> объекта</text> <object_comment/><text>"</text>
<text>
</text>
<text>WITH show current := TRUE
</text>
<text> WITH attachment :=</text> <attribute_name/><text> OF Ex_</text> <object_name/>
<text>
</text>
<text>WITH location := 10,100,700,230
</text>
<text>
</text>
<text>INSTANCE need_Class_</text><object_name/><text>_</text>
<attribute_name/> <text> ISA display</text>
<text>
</text>
<text>WITH wait := TRUE
</text>
<text>WITH delay changes := TRUE
</text>
<text>WITH items[1 ] := CheckGroup_Class_</text>
<object_name/><text>_</text><attribute_name/> 
<text>
</text>
<text>WITH items[2 ] := go_button
</text>
<text>WITH menus[1 ] := UNDETERMINED
</text>
</then>
<else>
</else>
</if_type_symbolic> 
<comment>The End Of Visualisation Of Symbolic Type</comment>
<comment>The Begin Of Visualisation Of Numeric Type</comment>
<if_type_numeric>
<then>
<text>INSTANCE ValueBox_Class_</text><object_name/>
<text>_</text><attribute_name/><text> ISA valuebox</text>
<text>
</text>
<text>WITH justify IS left
</text>
<text>WITH font := "System"
</text>
<text>WITH frame := TRUE
</text>
<text>WITH clipped := TRUE
</text>
<text>WITH attachment :=</text> <attribute_name/><text> OF Ex_</text><object_name/>
<text>
</text>
<text>WITH location := 10,10,200,100
</text>
<text>
</text>
<text>INSTANCE TextBox_Class_</text><object_name/><text>_</text><attribute_name/> <text> ISA textbox</text>
<text>
</text>
<text>WITH justify IS left
</text>
<text>WITH font := "System"
</text>
<text> WITH text := "Значение атрибута </text> <type_comment/><text> объекта </text> <object_comment/> <text>"</text>
<text>
</text>
<text>WITH location := 0,0,110,10
</text>
<text>
</text>
<text>INSTANCE need_Class_</text> <object_name/><text>_</text><attribute_name/><text> ISA display</text>
<text>
</text>
<text>WITH wait := TRUE
</text>
<text>WITH delay changes := TRUE
</text>
<text>WITH items[1 ] := TextBox_Class_</text> <object_name/><text>_</text><attribute_name/>
<text>
</text> 
<text>WITH items[2 ] := ValueBox_Class_</text><object_name/><text>_</text><attribute_name/>
<text>
</text> 
<text>WITH items[2 ] := go_button
</text>
<text>WITH menus[1 ] := UNDETERMINED
</text>
</then>
<else>
</else>
</if_type_numeric>
 <comment>The End Of Visualisation Of Numeric Type</comment>

<comment>The Begin Of Visualisation Of Fuzzy Type</comment>
<comment>Вот здесь я не знаю точно пока что делать...можно его просто проигнорировать, а можно .....вообщем пока так просто влепил стринг, а там посмотрим</comment>
<if_type_fuzzy>
<then>
<comment>Сюда пока ничего не придумал...Ибо тип этот Л5О не поддерживает</comment>
</then>
<else>
</else>
</if_type_fuzzy>	
<comment>The End Of Visualisation Of Fuzzy Type</comment>
</then>
<else>
</else>
</change> 
</loop_types>
<text>
</text>
</object_loop_attributes>
</loop_objects>

<comment>Здесь конец попыток визуализировать все экземпляры</comment>

<comment> чисто теоретически мы должны получить конструкцию похожую не правду...</comment>
<text>
</text>

<comment> Здесь описываются правила</comment>
<loop_rules>
<text>RULE  </text>
<rule_name/>
<text>
</text>
<text>IF </text>
<rule_make_logic_value and="AND" or="OR"  xor=" " not="NOT" and_var="infix" or_var="infix" xor_var="infix" comma="" obracket="(" cbracket=")">
<separator></separator>
<comment>-------------------------------------------------------</comment>
<if_numeric_assertion>
<then>
<nassertion_attribute>
<attribute_name/>
</nassertion_attribute>
<text> OF </text>
<nassertion_object>
<text>Ex_</text>
<object_name/>
</nassertion_object>
<nassertion_operation/>
<nassertion_make_expression add="+" sub="-" mul="*" div="/" power="^" neg="-" add_var="infix" sub_var="infix" mul_var="infix" power_var="infix" div_var="infix" comma="," obracket="(" cbracket=")">
<if_number>
<then>
<number_value/>
</then>
</if_number>
<if_numeric_value>
<then>
<nv_attribute>
<attribute_name/>
</nv_attribute>
<text> OF  </text>
<nv_object>
<object_name/>
<text>Ex_</text>
</nv_object>
</then>
</if_numeric_value>
</nassertion_make_expression>
<assertion_loop_properties>
<if_prop_interval>
<then>
<text></text>
<comment>pi_belief_value/</comment>
</then>
</if_prop_interval>
</assertion_loop_properties>
<assertion_loop_properties>
<if_prop_accuracy>
<then>
<text></text>
</then>
</if_prop_accuracy>
</assertion_loop_properties>
</then>
 </if_numeric_assertion>
<comment>------------------------------------------</comment>
<comment>Если символьная конструкция</comment>
<if_symbolic_assertion>
<then>
<sassertion_attribute>
<attribute_name/>
</sassertion_attribute>

<text> OF </text>
<sassertion_object>
<text>Ex_</text>
<object_name/>
</sassertion_object>		
<text> IS "</text>
<sassertion_value/>
<text>"</text>
<assertion_loop_properties>
<if_prop_interval>
<then>
<text></text>
<comment>pi_belief_value/</comment>
</then>
</if_prop_interval>
</assertion_loop_properties>
</then>
 </if_symbolic_assertion>
<comment>А ещё лажа полная::::: ваще нету отношений в L5O </comment>
</rule_make_logic_value>
<comment> -----------------END IF-----------------------------</comment>
<text>
</text>
<text>
THEN
</text>
<rule_loop_asser_then>
<if_numeric_assertion>
<then>
<nassertion_attribute>
<attribute_name/>
</nassertion_attribute>
<text> OF </text>
<nassertion_object>
<text>Ex_</text>
<object_name/>
</nassertion_object>
<text> := </text>
<comment> здесь кроме этого ничего не получится nassertion_operation/ ></comment>
<nassertion_make_expression add="+" sub="-" mul="*" div="/" power="^" neg="-" add_var="infix" sub_var="infix" mul_var="infix" power_var="infix" div_var="infix" comma="," obracket="(" cbracket=")">
<if_number>
<then>
<number_value/>
</then>
</if_number>
<if_numeric_value>
<then>
<nv_attribute>
<attribute_name/>
</nv_attribute>
<text> OF </text>
<nv_object>
<text>Ex_</text>
<object_name/>
</nv_object>
</then>
</if_numeric_value>
</nassertion_make_expression>
<text></text>
<assertion_loop_properties>
<if_prop_interval>
<then>
 <text> CF </text>
<pi_belief_value/>
</then>
</if_prop_interval>
</assertion_loop_properties>
<text>
</text>
</then>
</if_numeric_assertion>

 <if_symbolic_assertion>
 <then>
<sassertion_attribute>
<attribute_name/>
</sassertion_attribute>
<text> OF </text>
<sassertion_object>
<text>Ex_</text>
<object_name/>
</sassertion_object>
<text> :="</text>
<sassertion_value/>
<text>"</text>
<assertion_loop_properties>
<if_prop_interval>
<then>
<text> CF </text>
<pi_belief_value/>
</then>
</if_prop_interval>
</assertion_loop_properties>
<text>
</text>
</then>
</if_symbolic_assertion>
</rule_loop_asser_then>
<comment>--------------------END OF THEN ---------------------------------</comment>
<if_rule_has_else>
<then>
<text>
</text>
 <text>ELSE </text>
<rule_loop_asser_else>
<if_numeric_assertion>
<then>
<nassertion_attribute>
<attribute_name/>
</nassertion_attribute>
<text> OF </text>
<nassertion_object>
<text>Ex_</text><object_name/>
</nassertion_object>
<comment> Тоже что и в THEN nassertion_operation/>
</comment>
<text>:= </text>
<nassertion_make_expression add="+" sub="-" mul="*" div="/" power="^" neg="-" add_var="infix" sub_var="infix" mul_var="infix" power_var="infix" div_var="infix" comma="," obracket="(" cbracket=")">
<if_number>
<then>
<number_value/>
</then>
</if_number>
<if_numeric_value>
<then>
<nv_attribute>
<attribute_name/>
</nv_attribute>
<text> OF </text>
<nv_object>
<text>Ex_</text> <object_name/>
</nv_object>
</then>
</if_numeric_value>
</nassertion_make_expression>
<text></text>
<assertion_loop_properties>
<if_prop_interval>
<then>
<text> CF </text>
<pi_belief_value/>
</then>
</if_prop_interval>
</assertion_loop_properties>
<text>
</text>
</then>
</if_numeric_assertion>
<if_symbolic_assertion>
<then>
<sassertion_attribute>
<attribute_name/>
</sassertion_attribute>
<text> OF </text>
<sassertion_object>
<text>Ex_</text><object_name/>
</sassertion_object>
<text>:= "</text>
<sassertion_value/>
<text>"</text>
<assertion_loop_properties>
<if_prop_interval>
<then>
<text> CF </text>
<pi_belief_value/>
</then>
</if_prop_interval>
</assertion_loop_properties>
<text>
</text>
</then>
</if_symbolic_assertion>
</rule_loop_asser_else>
</then>
</if_rule_has_else>
<text>КОММЕНТАРИЙ </text>
<rule_comment/>
<text>
</text>
</loop_rules>
<text>
</text>
<text>END</text>
</format_file>
