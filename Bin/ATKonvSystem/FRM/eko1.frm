<format_file>
 <part file_ext="tex" encoding="dos"> 
  <text>МОДЕЛЬ = Model_From_AT</text>
   <text>;
{
РАЗРЕШЕНИЕ_КОНФЛИКТОВ = ПЕРВОЕ_ПРАВИЛО;
---------------------------------------
-- СЦЕНАРИЙ
---------------------------------------
СЦЕНАРИЙ = 1;
{
    ДЕЙСТВИЕ = РЕЗУЛЬТАТ;
    ПАРАМЕТРЫ = ;
}
---------------------------------------
-- СИМВОЛЬНЫЕ АТРИБУТЫ
---------------------------------------
</text>   
<loop_objects>
  <object_loop_attributes>
     <attribute_type>

<comment> ДЛЯ ЧИСЛОВЫХ АТРИБУТОВ </comment>
        <if_type_numeric>
            <then>
                  <text>ЧИСЛОВОЙ_АТРИБУТ = ATR</text>
                  <object_number/>
                  <text>_</text>
                  <attribute_number/> 
                  <text>;
{
</text>
                  <text>    РАЗВЕРНУТОЕ_ИМЯ = "</text>
                   <type_comment/><text>";
</text>
                  <text>    ОТ = </text>
                  <type_min/>
                  <text>;
</text>
                  <text>    ДО = </text>
                  <type_max/>
                  <text>;
}
</text>
            </then>
        </if_type_numeric>

<comment> ДЛЯ СИМВОЛЬНЫХ АТРИБУТОВ </comment>
        <if_type_symbolic>
           <then>
               <text>СИМВОЛЬНЫЙ_АТРИБУТ = ATR</text>
               <object_number/>
               <text>_</text>
               <attribute_number/>
               <text>;
{
</text>
               <text>    РАЗВЕРНУТОЕ_ИМЯ = "</text>
               <type_comment/>
               <text>";
</text>
               <type_loop_values>
                  <text>   ЗНАЧЕНИЕ = "ZN</text>
                  <type_value_number/>
                  <text>";
   {
</text>
                  <text>     РАЗВЕРНУТОЕ_ИМЯ = "</text>
                  <type_value/>
                 <text>";
     ИМЯ_УТВЕРЖДЕНИЯ = "ATR</text>
                 <object_number/>
                 <text>_</text>
                 <attribute_number/>
                 <text>_</text>
                 <type_value_number/>
<text>"; 
     ПО_УМОЛЧАНИЮ = 0.00;
   }
</text>
               </type_loop_values>
               <text>}
</text>
           </then>
        </if_type_symbolic>
     </attribute_type>
  </object_loop_attributes>
</loop_objects>


<loop_rules>
    <rule_loop_asser_then>
         <if_symbolic_assertion>
<comment> СОЗДАЕМ ЛОГИЧЕСКОЕ ПРАВИЛО </comment>
                <then> 
                      <text>ПРАВИЛО = L</text>
                      <rule_number/>
                      <text>_</text>
                      <sassertion_object>
                           <sassertion_attribute>
                                 <object_number/>
                                 <text>_</text>
                                 <attribute_number/>
                                 <text>_</text>
                                  <attribute_type>
                                       <type_value_number_equal>
                                             <sassertion_value/>
                                      </type_value_number_equal>
                                  </attribute_type>                               
                                 <text>;
{
   ТИП_ПРАВИЛА = ЛОГИЧЕСКОЕ;
   КОММЕНТАРИЙ = "</text>
                                 <rule_comment/>
                                 <text>";
   ЦЕЛЬ = ATR</text>
                              <object_number/>
                              <text>_</text>
                              <attribute_number/>
                              <text>_</text>
                              <attribute_type>
                                    <type_value_number_equal>
                                           <sassertion_value/>
                                    </type_value_number_equal>
                              </attribute_type>
                              <text>;
   ЕСЛИ = </text>
                              <rule_make_logic_value and="&#38;" or="|" xor=" " not="~" and_var="infix" or_var="infix" xor_var="infix" comma="" obracket="(" cbracket=")">
                                       <separator></separator>
                                       <group_separator>
</group_separator>                                       <if_numeric_assertion>
                                            <then>
                                                  <text>(</text>   
                                                  <nassertion_object>
                                                       <nassertion_attribute>
                                                             <text>ATR</text>
                                                             <object_number/>
                                                             <text>_</text>
                                                             <attribute_number/>
                                                       </nassertion_attribute>
                                                  </nassertion_object>
                                                  <nassertion_operation/>
                                                  <nassertion_make_expression add="+" sub="-" mul="*" div="/" power="exp" neg="-" add_var="infix" sub_var="infix" mul_var="infix" div_var="infix" power_var="prefix" comma="," obracket="(" cbracket=")">
                                                      <if_number>
                                                            <then>
                                                                  <number_value/>
                                                            </then>
                                                      </if_number>
                                                      <if_numeric_value>
                                                           <then>
                                                                 <nv_object>
                                                                     <nv_attribute>
                                                                           <text>ATR</text>
                                                                           <object_number/>
                                                                           <text>_</text>
                                                                           <attribute_number/>
                                                                     </nv_attribute>
                                                                 </nv_object>
                                                           </then>
                                                      </if_numeric_value>
                                                  </nassertion_make_expression> 
                                                  <text>)</text>
                                            </then>
                                       </if_numeric_assertion>
                                       <if_symbolic_assertion>
                                            <then>
                                                  <sassertion_object>
                                                        <sassertion_attribute>
                                                             <text>ATR</text>
                                                             <object_number/>
                                                             <text>_</text>
                                                             <attribute_number/>
                                                             <text>_</text>
                                                             <attribute_type>
                                                                   <type_value_number_equal>
                                                                        <sassertion_value/>
                                                                   </type_value_number_equal>
                                                             </attribute_type>
                                                          </sassertion_attribute>
                                                  </sassertion_object>
                                            </then>
                                       </if_symbolic_assertion>
                               </rule_make_logic_value>
                           </sassertion_attribute>
                      </sassertion_object>
                     <text>;
  ТО = 1.0;
}
</text>  
              </then>
         </if_symbolic_assertion>

         <if_numeric_assertion>
<comment> СОЗДАЕМ АРИФМЕТИЧЕСКОЕ ПРАВИЛО </comment>
                <then>
                      <text>ПРАВИЛО = N</text>
                      <rule_number/>
                      <text>_</text>
                      <nassertion_object>
                           <nassertion_attribute>
                                 <object_number/>
                                 <text>_</text>
                                 <attribute_number/>
                                 <text>;
{
   ТИП_ПРАВИЛА = АРИФМЕТИЧЕСКОЕ;
   КОММЕНТАРИЙ = "</text>
                                 <rule_comment/>
                                 <text>";
   ЦЕЛЬ = ATR</text>
                              <object_number/>
                              <text>_</text>
                              <attribute_number/>
                              <text>_</text>
                              <text>;
   ЕСЛИ = 
</text>
                              <rule_make_logic_value and="&#38;" or="|" xor=" " not="~" and_var="infix" or_var="infix" xor_var="infix" comma="" obracket="(" cbracket=")">
                                       <separator>    </separator>
                                       <if_numeric_assertion>
                                            <then>
                                                  <text>(</text>   
                                                  <nassertion_object>
                                                       <nassertion_attribute>
                                                             <text>ATR</text>
                                                             <object_number/>
                                                             <text>_</text>
                                                             <attribute_number/>
                                                       </nassertion_attribute>
                                                  </nassertion_object>
                                                  <nassertion_operation/>
                                                  <nassertion_make_expression add="+" sub="-" mul="*" div="/" power="exp" neg="-" add_var="infix" sub_var="infix" mul_var="infix" div_var="infix" power_var="prefix" comma="," obracket="(" cbracket=")">
                                                      <if_number>
                                                            <then>
                                                                  <number_value/>
                                                            </then>
                                                      </if_number>
                                                      <if_numeric_value>
                                                           <then>
                                                                 <nv_object>
                                                                     <nv_attribute>
                                                                           <text>ATR</text>
                                                                           <object_number/>
                                                                           <text>_</text>
                                                                           <attribute_number/>
                                                                     </nv_attribute>
                                                                 </nv_object>
                                                           </then>
                                                      </if_numeric_value>
                                                  </nassertion_make_expression> 
                                                  <text>)</text>
                                            </then>
                                       </if_numeric_assertion>
                                       <if_symbolic_assertion>
                                            <then>
                                                  <sassertion_object>
                                                        <sassertion_attribute>
                                                             <text>ATR</text>
                                                             <object_number/>
                                                             <text>_</text>
                                                             <attribute_number/>
                                                             <text>_</text>
                                                             <attribute_type>
                                                                   <type_value_number_equal>
                                                                        <sassertion_value/>
                                                                   </type_value_number_equal>
                                                             </attribute_type>
                                                          </sassertion_attribute>
                                                  </sassertion_object>
                                            </then>
                                       </if_symbolic_assertion>
                               </rule_make_logic_value>
                           </nassertion_attribute>
                      </nassertion_object>
                     <text>;
  ТО = </text>  
                      <nassertion_make_expression add="+" sub="-" mul="*" div="/" power="exp" neg="-" add_var="infix" sub_var="infix" mul_var="infix" div_var="infix" power_var="prefix" comma="," obracket="(" cbracket=")">
                                <if_number>
                                          <then>
                                                  <number_value/>
                                          </then>
                                </if_number>
                                <if_numeric_value>
                                           <then>
                                                   <nv_object>
                                                         <nv_attribute>
                                                               <text>ATR</text>
                                                                <object_number/>
                                                                 <text>_</text>
                                                                 <attribute_number/>
                                                          </nv_attribute>
                                                    </nv_object>
                                            </then>
                                </if_numeric_value>
                        </nassertion_make_expression>                
                     <text>;
</text>
               </then>
         </if_numeric_assertion>
    </rule_loop_asser_then>

<comment>СОЗДАЕМ АЛЬТЕРНАТИВНУЮ ЧАСТЬ ПРАВИЛА, Т.Е. ЕСЛИ У ПРАВИЛА БЫЛА ELSE -ЧАСТЬ</comment>
    <rule_loop_asser_else>
         <if_symbolic_assertion>
<comment> СОЗДАЕМ ЛОГИЧЕСКОЕ ПРАВИЛО </comment>
                <then> 
                      <text>ПРАВИЛО = LN</text>
                      <rule_number/>
                      <text>_</text>
                      <sassertion_object>
                           <sassertion_attribute>
                                 <object_number/>
                                 <text>_</text>
                                 <attribute_number/>
                                 <text>_</text>
                                  <attribute_type>
                                       <type_value_number_equal>
                                             <sassertion_value/>
                                      </type_value_number_equal>
                                  </attribute_type>                               
                                 <text>;
{
   ТИП_ПРАВИЛА = ЛОГИЧЕСКОЕ;
   КОММЕНТАРИЙ = "</text>
                                 <rule_comment/>
                                 <text>";
   ЦЕЛЬ = ATR</text>
                              <object_number/>
                              <text>_</text>
                              <attribute_number/>
                              <text>_</text>
                              <attribute_type>
                                    <type_value_number_equal>
                                           <sassertion_value/>
                                    </type_value_number_equal>
                              </attribute_type>
                              <text>;
   ЕСЛИ = 
~(</text>
                              <rule_make_logic_value and="&#38;" or="|" xor=" " not="~" and_var="infix" or_var="infix" xor_var="infix" comma="" obracket="(" cbracket=")">
                                       <separator>    </separator>
                                       <if_numeric_assertion>
                                            <then>
                                                  <text>(</text>   
                                                  <nassertion_object>
                                                       <nassertion_attribute>
                                                             <text>ATR</text>
                                                             <object_number/>
                                                             <text>_</text>
                                                             <attribute_number/>
                                                       </nassertion_attribute>
                                                  </nassertion_object>
                                                  <nassertion_operation/>
                                                  <nassertion_make_expression add="+" sub="-" mul="*" div="/" power="exp" neg="-" add_var="infix" sub_var="infix" mul_var="infix" div_var="infix" power_var="prefix" comma="," obracket="(" cbracket=")">
                                                      <if_number>
                                                            <then>
                                                                  <number_value/>
                                                            </then>
                                                      </if_number>
                                                      <if_numeric_value>
                                                           <then>
                                                                 <nv_object>
                                                                     <nv_attribute>
                                                                           <text>ATR</text>
                                                                           <object_number/>
                                                                           <text>_</text>
                                                                           <attribute_number/>
                                                                     </nv_attribute>
                                                                 </nv_object>
                                                           </then>
                                                      </if_numeric_value>
                                                  </nassertion_make_expression> 
                                                  <text>)</text>
                                            </then>
                                       </if_numeric_assertion>
                                       <if_symbolic_assertion>
                                            <then>
                                                  <sassertion_object>
                                                        <sassertion_attribute>
                                                             <text>ATR</text>
                                                             <object_number/>
                                                             <text>_</text>
                                                             <attribute_number/>
                                                             <text>_</text>
                                                             <attribute_type>
                                                                   <type_value_number_equal>
                                                                        <sassertion_value/>
                                                                   </type_value_number_equal>
                                                             </attribute_type>
                                                          </sassertion_attribute>
                                                  </sassertion_object>
                                            </then>
                                       </if_symbolic_assertion>
                               </rule_make_logic_value>
                           </sassertion_attribute>
                      </sassertion_object>
                     <text>);
  ТО = 1.0;
}
</text>  
              </then>
         </if_symbolic_assertion>

         <if_numeric_assertion>
<comment> СОЗДАЕМ АРИФМЕТИЧЕСКОЕ ПРАВИЛО </comment>
                <then>
                      <text>ПРАВИЛО = NN</text>
                      <rule_number/>
                      <text>_</text>
                      <nassertion_object>
                           <nassertion_attribute>
                                 <object_number/>
                                 <text>_</text>
                                 <attribute_number/>
                                 <text>;
{
   ТИП_ПРАВИЛА = АРИФМЕТИЧЕСКОЕ;
   КОММЕНТАРИЙ = "</text>
                                 <rule_comment/>
                                 <text>";
   ЦЕЛЬ = ATR</text>
                              <object_number/>
                              <text>_</text>
                              <attribute_number/>
                              <text>_</text>
                              <text>;
   ЕСЛИ = 
~(</text>
                              <rule_make_logic_value and="&#38;" or="|" xor=" " not="~" and_var="infix" or_var="infix" xor_var="infix" comma="" obracket="(" cbracket=")">
                                       <separator>    </separator>
                                       <if_numeric_assertion>
                                            <then>
                                                  <text>(</text>   
                                                  <nassertion_object>
                                                       <nassertion_attribute>
                                                             <text>ATR</text>
                                                             <object_number/>
                                                             <text>_</text>
                                                             <attribute_number/>
                                                       </nassertion_attribute>
                                                  </nassertion_object>
                                                  <nassertion_operation/>
                                                  <nassertion_make_expression add="+" sub="-" mul="*" div="/" power="exp" neg="-" add_var="infix" sub_var="infix" mul_var="infix" div_var="infix" power_var="prefix" comma="," obracket="(" cbracket=")">
                                                      <if_number>
                                                            <then>
                                                                  <number_value/>
                                                            </then>
                                                      </if_number>
                                                      <if_numeric_value>
                                                           <then>
                                                                 <nv_object>
                                                                     <nv_attribute>
                                                                           <text>ATR</text>
                                                                           <object_number/>
                                                                           <text>_</text>
                                                                           <attribute_number/>
                                                                     </nv_attribute>
                                                                 </nv_object>
                                                           </then>
                                                      </if_numeric_value>
                                                  </nassertion_make_expression> 
                                                  <text>)</text>
                                            </then>
                                       </if_numeric_assertion>
                                       <if_symbolic_assertion>
                                            <then>
                                                  <sassertion_object>
                                                        <sassertion_attribute>
                                                             <text>ATR</text>
                                                             <object_number/>
                                                             <text>_</text>
                                                             <attribute_number/>
                                                             <text>_</text>
                                                             <attribute_type>
                                                                   <type_value_number_equal>
                                                                        <sassertion_value/>
                                                                   </type_value_number_equal>
                                                             </attribute_type>
                                                          </sassertion_attribute>
                                                  </sassertion_object>
                                            </then>
                                       </if_symbolic_assertion>
                               </rule_make_logic_value>
                           </nassertion_attribute>
                      </nassertion_object>
                     <text>);
  ТО = </text>  
                      <nassertion_make_expression add="+" sub="-" mul="*" div="/" power="exp" neg="-" add_var="infix" sub_var="infix" mul_var="infix" div_var="infix" power_var="prefix" comma="," obracket="(" cbracket=")">
                                <if_number>
                                          <then>
                                                  <number_value/>
                                          </then>
                                </if_number>
                                <if_numeric_value>
                                           <then>
                                                   <nv_object>
                                                         <nv_attribute>
                                                               <text>ATR</text>
                                                                <object_number/>
                                                                 <text>_</text>
                                                                 <attribute_number/>
                                                          </nv_attribute>
                                                    </nv_object>
                                            </then>
                                </if_numeric_value>
                        </nassertion_make_expression>                
                     <text>;
</text>
                </then>
         </if_numeric_assertion>
    </rule_loop_asser_else>
</loop_rules>

<comment>ЗДЕСЬ ВСТАВЛЯЕМ СПИСОК АЛЬТЕРНАТИВНЫХ ВОПРОСОВ ДЛЯ ВСЕХ 
АТРИБУТОВ, но МОЖНО СДЕЛАТЬ ДИАЛОГ И СПРАШИВАТЬ У ПОЛЬЗОВАТЕЛЯ ДЛЯ ЧЕГО НАДО ДЕЛАТЬ, А ДЛЯ ЧЕГО НЕТ. 
</comment>
<loop_objects>
    <object_loop_attributes>
          <text>ПРАВИЛО = A</text>
          <object_number/>
          <text>_</text>
          <attribute_number/>
          <text>;
{
      ТИП_ПРАВИЛА = АЛЬТЕРНАТИВНЫЙ_ВОПРОС;
      ЦЕЛЬ = ATR</text>
      <object_number/>
      <text>_</text>
      <attribute_number/>
      <text>;
       ТО = ;
}
</text>
    </object_loop_attributes>
</loop_objects>
<text>}
</text>
</part>
</format_file>
