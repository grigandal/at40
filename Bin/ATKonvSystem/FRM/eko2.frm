<format_file>
 <part file_ext="tex" encoding="dos"> 
  <text>МОДЕЛЬ = </text>
   <problem_name/>
   <text>;
{
РАЗРЕШЕНИЕ_КОНФЛИКТОВ = ПЕРВОЕ_ПРАВИЛО;
---------------------------------------
-- СЦЕНАРИЙ
---------------------------------------
СЦЕНАРИЙ = 1;
{
    ДЕЙСТВИЕ = ЦЕЛЬ;
    ПАРАМЕТРЫ = СОВЕТ;
}
---------------------------------------
-- СИМВОЛЬНЫЕ АТРИБУТЫ
---------------------------------------
</text>   
<loop_objects>
  <object_loop_attributes>
     <attribute_type>
        <if_type_symbolic>
           <then>
               <text>СИМВОЛЬНЫЙ_АТРИБУТ = ATR</text>
               <object_number/>
               <text>_</text>
               <attribute_number/>
               <text>;
{
</text>
               <type_loop_values>
                  <text>   ЗНАЧЕНИЕ = </text>
                  <type_value/>
                  <text>;
   {
      ПО_УМОЛЧАНИЮ = 0.00
   }
</text>
               </type_loop_values>
               <text>}
</text>
           </then>
           <else>
               <message type="info">В поле знаний содержаться не символьные типы! Их преобразование не поддержтвается!</message>
           </else>
        </if_type_symbolic>
     </attribute_type>
  </object_loop_attributes>
</loop_objects>

<text>СИМВОЛЬНЫЙ_АТРИБУТ = СОВЕТ;
{
</text>
<loop_rules>
   <rule_loop_asser_then>
      <if_symbolic_assertion>
         <then>
            <sassertion_object>
               <sassertion_attribute>
                  <text>    ЗНАЧЕНИЕ = D</text>
                  <object_number/>
                  <text>_</text>
                  <attribute_number/>
                  <text>
    {
       ПО_УМОЛЧАНИЮ = 0.00;
    }
</text>
               </sassertion_attribute>
            </sassertion_object>
         </then>
         <else>
            <message type="info">В поле знаний содержаться не символьные типы! Их преобразование не поддержтвается!</message>
         </else>
      </if_symbolic_assertion>
   </rule_loop_asser_then>
</loop_rules>
<text>}
---------------------------------------
-- ПРАВИЛА
-- СИМВОЛЬНЫЕ АТРИБУТЫ
---------------------------------------
</text>

<loop_rules>
  <text>ПРАВИЛО = Q</text>
  <rule_number/>
  <text>;
{</text>
  <rule_loop_asser_then>
     <if_symbolic_assertion>
        <then>
            <text>  ЦЕЛЬ = [СОВЕТ.D</text>
           <sassertion_object>
              <sassertion_attribute>
                  <object_number/>
                  <text>_</text>
                  <attribute_number/>
                  <text>];
</text>
              </sassertion_attribute>
           </sassertion_object>
        </then>
        <else>
          <message type="info">В поле знаний содержаться не символьные типы! Их преобразование не поддержтвается!</message>
        </else>
     </if_symbolic_assertion>
  </rule_loop_asser_then>
  <text>ТИП_ПРАВИЛА = ПРОСТОЙ_ВОПРОС;
ЕСЛИ = 
</text>
  <rule_loop_asser_if>
     <if_symbolic_assertion>
        <then>
            <text>  [ATR</text>
           <sassertion_object>
              <sassertion_attribute>
                  <object_number/>
                  <text>_</text>
                  <attribute_number/>
                  <text>.</text>
                  <sassertion_value/>
                  <text>]</text>
                  <if_assertion_last type="symbolic">
                     <then>
                       <text>;
</text>
                     </then>
                     <else>
                       <text> &#38;
</text>
                     </else>
                  </if_assertion_last>
              </sassertion_attribute>
           </sassertion_object>
        </then>
        <else>
          <message type="info">В поле знаний содержаться не символьные типы! Их преобразование не поддержтвается!</message>
        </else>
     </if_symbolic_assertion>
  </rule_loop_asser_if>
  <text>   ТО = </text>
  <rule_loop_asser_then>
     <if_symbolic_assertion>
        <then>
           <sassertion_value/>
        </then>
        <else>
          <message type="info">В поле знаний содержаться не символьные типы! Их преобразование не поддержтвается!</message>
        </else>
     </if_symbolic_assertion>
  </rule_loop_asser_then>
  <text>}
</text>
</loop_rules>

<loop_objects>
  <object_loop_attributes>
    <text>ПРАВИЛО = A</text>
    <object_number/>
    <text>_</text>
    <attribute_number/>
    <text>;
{ 
    ЦЕЛЬ = ATR</text>
    <object_number/>
    <text>_</text>
    <attribute_number/>
    <text>;
    ТИП_ПРАВИЛА = АЛЬТЕРНАТИВНЫЙ_ВОПРОС;
    ТО = </text>
    <attribute_type>
      <type_comment/>
      <text>;
}
</text>
    </attribute_type>
  </object_loop_attributes>
</loop_objects>
<text>}</text>
  </part>
</format_file>
