<format_file>
   <part file_ext="prl" encoding="">
         <options>
            <change_values all="true" types_symbolic="" assertions_symbolic="">
               <change_substr>
                   <remove_substr>
                      <text>(</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>)</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>.</text>
                   </remove_substr>
                   <insert_substr>
                      <text></text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>,</text>
                   </remove_substr>
                   <insert_substr>
                      <text></text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>!</text>
                   </remove_substr>
                   <insert_substr>
                      <text></text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>"</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>#</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>%</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>^</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>*</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text> </text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>&#38;</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>+</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>=</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>\</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>|</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>:</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>;</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>~</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>`</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>'</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>{</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>}</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>[</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>]</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>&#252;</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>@</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>&#62;</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>&#60;</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>?</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>

               <change_substr>
                   <remove_substr>
                      <text>-</text>
                   </remove_substr>
                   <insert_substr>
                      <text>_</text>
                   </insert_substr>
               </change_substr>
            </change_values>
         </options>
         <text>
$VERSION35
$LOCATIONS ARE CHARACTERS
</text>
      <loop_objects>
             <text>
CLASS Obj</text>
             <object_number/>
             <text>
</text>
             <object_loop_attributes>
                   <attribute_type>
                         <comment>ЕСЛИ АТРИБУТ СИМВОЛЬНОГО ТИПА - ФОРМИРУЕМ АТРИБУТ ТИПА COMPOUND</comment>
                         <if_type_symbolic>
                               <then>
                                     <text>    WITH ATR</text>
                                     <attribute_number/>
                                     <text> COMPOUND</text>
                                     <type_loop_values>
                                           <text>
         _</text>
                                           <type_value/>
                                           <if_type_value_last>
                                                <then/>
                                                <else>
                                                      <text>,</text>
                                                </else>
                                           </if_type_value_last>
                                     </type_loop_values>
                                     <text>
         QUERY FROM disp</text>
                                     <object_number/>
                                     <text>_</text>
                                     <attribute_number/>
                                     <text>
         SEARCH ORDER CONTEXT WHEN NEEDED RULES QUERY DEFAULT
</text>
                               </then>
                         </if_type_symbolic>
                         <if_type_numeric>
                               <then>
                                      <text>    WITH ATR</text>
                                      <attribute_number/>
                                      <text> NUMERIC
         QUERY FROM disp</text>
                                      <object_number/>
                                      <text>_</text>
                                      <attribute_number/>
                                     <text>
         SEARCH ORDER CONTEXT WHEN NEEDED RULES QUERY DEFAULT
</text>
                               </then>
                         </if_type_numeric>
                   </attribute_type>
             </object_loop_attributes>
      </loop_objects>

<comment>СОЗДАЕМ ДИСПЛЕИ</comment>
      <text>

INSTANCE the application ISA application
  WITH unknowns fail := TRUE
  WITH threshold := 50
  WITH title display := first_disp
  WITH conclusion display := last_disp
  WITH ignore breakpoints := FALSE
  WITH reasoning on := FALSE
  WITH numeric precision := 8
  WITH demon strategy IS fire first
 
INSTANCE first_disp ISA display
  WITH wait := TRUE
  WITH delay changes := TRUE
  WITH items {1 } := ok_button
  WITH menus {1 } := UNDETERMINED
 
INSTANCE last_disp ISA display
  WITH wait := TRUE
  WITH delay changes := TRUE
  WITH items {1 } := restart_button
  WITH menus {1 } := UNDETERMINED

</text>
        <loop_objects>
             <object_loop_attributes>
                    <text>INSTANCE disp</text>
                    <object_number/>
                    <text>_</text>
                    <attribute_number/>
                    <text> ISA display
    WITH wait := TRUE
    WITH delay changes := TRUE
    WITH items{1 } := </text>
                   <attribute_type>
                        <if_type_symbolic>
                              <then>
                                     <text>rg</text>                                  
                              </then>
                        </if_type_symbolic>
                        <if_type_numeric>
                              <then>
                                     <text>pb</text>                                  
                              </then>
                        </if_type_numeric>
                   </attribute_type>
                   <object_number/>
                   <text>_</text>
                   <attribute_number/>
                   <text>
    WITH items{2 } := ok_button
    WITH menus{1 } := UNDETERMINED

</text>
             </object_loop_attributes>
        </loop_objects>

       <loop_objects>
            <object_loop_attributes>
                  <attribute_type>
                       <if_type_numeric> 
                             <then>
                                    <text>INSTANCE pb</text>
                                    <object_number/> 
                                    <text>_</text>
                                    <attribute_number/>
                                    <text> ISA promptbox
  WITH font := "System"
  WITH justify IS left
  WITH frame := TRUE
  WITH show current := TRUE
  WITH attachment := ATR</text>
                                    <attribute_number/>
                                    <text> OF Obj</text>
                                    <object_number/>
                                    <text>
  WITH location := 4,4,29,5
</text>
                             </then>
                       </if_type_numeric> 
                  </attribute_type>
            </object_loop_attributes>
       </loop_objects>

<text>
INSTANCE ok_button ISA pushbutton
  WITH label := "Дальше"
  WITH attribute attachment := continue display OF main window
  WITH location := 3,2,17,4

INSTANCE restart_button ISA pushbutton
  WITH label := "Повторить"
  WITH attribute attachment := restart OF the application
  WITH location := 3,2,17,4
</text>

       <loop_objects>
            <object_loop_attributes>
                  <attribute_type>
                       <if_type_symbolic> 
                             <then>
                                    <text>
INSTANCE rg</text>
                                    <object_number/> 
                                    <text>_</text>
                                    <attribute_number/>
                                    <text> ISA radiobutton group
  WITH frame := TRUE
  WITH group label := "</text>
                                    <type_comment/>
                                    <text>"
  WITH show current := TRUE
  WITH attachment := ATR</text>
                                    <attribute_number/>
                                    <text> OF Obj</text>
                                    <object_number/>
                                    <text>
  WITH location := 3,5,29,10
</text>
                             </then>
                       </if_type_symbolic> 
                  </attribute_type>
            </object_loop_attributes>
       </loop_objects>

       <loop_rules>
              <text>RULE </text>
              <rule_number/>
              <text>
IF </text>
            <rule_make_logic_value and=" AND " or=" OR " xor=" " not="NOT" and_var="infix" or_var="infix" xor_var="infix" comma="" obracket="(" cbracket=")">
                                       <separator></separator>
                                       <group_separator>
</group_separator>                                       <if_numeric_assertion>
                                            <then>
                                                  <text>(</text>   
                                                  <nassertion_object>
                                                       <nassertion_attribute>
                                                             <text>ATR</text>
                                                             <attribute_number/>
                                                             <text> OF Obj</text>
                                                             <object_number/>
                                                       </nassertion_attribute>
                                                  </nassertion_object>
                                                  <nassertion_operation/>
                                                  <nassertion_make_expression add="+" sub="-" mul="*" div="/" power="exp" neg="-" add_var="infix" sub_var="infix" mul_var="infix" div_var="infix" power_var="prefix" comma="," obracket="(" cbracket=")">
                                                      <if_number>
                                                            <then>
                                                                  <number_value/>
                                                            </then>
                                                      </if_number>
                                                      <if_numeric_value>
                                                           <then>
                                                                 <nv_object>
                                                                     <nv_attribute>
                                                                           <text>ATR</text>
                                                                           <attribute_number/>
                                                                           <text> OF Obj</text>
                                                                           <object_number/>
                                                                     </nv_attribute>
                                                                 </nv_object>
                                                           </then>
                                                      </if_numeric_value>
                                                  </nassertion_make_expression> 
                                                  <text>)</text>
                                            </then>
                                       </if_numeric_assertion>
                                       <if_symbolic_assertion>
                                            <then>
                                                  <sassertion_object>
                                                        <sassertion_attribute>
                                                             <text>ATR</text>
                                                             <attribute_number/>
                                                             <text> OF Obj</text>
                                                             <object_number/>
                                                             <text> IS _</text>
                                                             <sassertion_value/>
                                                          </sassertion_attribute>
                                                  </sassertion_object>
                                            </then>
                                       </if_symbolic_assertion>
                               </rule_make_logic_value>
                              <text>
THEN </text>
                             <rule_loop_asser_then>
                                   <if_symbolic_assertion>
                                        <then>
                                                 <text>ATR</text>
                                                 <sassertion_attribute>
                                                        <sassertion_object>
                                                             <attribute_number/>
                                                             <text> OF Obj</text>
                                                             <object_number/>
                                                             <text> IS _</text>
                                                             <sassertion_value/>
                                                        </sassertion_object>
                                                 </sassertion_attribute>
                                        </then>
                                   </if_symbolic_assertion>
                                  
                                   <if_numeric_assertion>
                                        <then>
                                                 <text>ATR</text>
                                                 <nassertion_attribute>
                                                        <nassertion_object>
                                                             <attribute_number/>
                                                             <text> OF Obj</text>
                                                             <object_number/>
                                                             <text> := </text>
                                                  <nassertion_make_expression add="+" sub="-" mul="*" div="/" power="exp" neg="-" add_var="infix" sub_var="infix" mul_var="infix" div_var="infix" power_var="prefix" comma="," obracket="(" cbracket=")">
                                                      <if_number>
                                                            <then>
                                                                  <number_value/>
                                                            </then>
                                                      </if_number>
                                                      <if_numeric_value>
                                                           <then>
                                                                 <nv_object>
                                                                     <nv_attribute>
                                                                           <text>ATR</text>
                                                                           <attribute_number/>
                                                                           <text> OF Obj</text>
                                                                           <object_number/>
                                                                     </nv_attribute>
                                                                 </nv_object>
                                                           </then>
                                                      </if_numeric_value>
                                                  </nassertion_make_expression> 
                                                        </nassertion_object>
                                                 </nassertion_attribute>
                                        </then>
                                   </if_numeric_assertion>
                                    <if_assertion_last type="all">
                                          <then/>
                                          <else>
                                                <text>
AND </text>
                                          </else>
                                    </if_assertion_last>
                             </rule_loop_asser_then>
                             <if_rule_has_else>
                                 <then>
                                       <text>
ELSE </text>
                                 </then>
                             </if_rule_has_else>
                             <rule_loop_asser_else>
                                   <if_symbolic_assertion>
                                        <then>
                                                 <text>ATR</text>
                                                 <sassertion_attribute>
                                                        <sassertion_object>
                                                             <attribute_number/>
                                                             <text> OF Obj</text>
                                                             <object_number/>
                                                             <text> IS _</text>
                                                             <sassertion_value/>
                                                        </sassertion_object>
                                                 </sassertion_attribute>
                                        </then>
                                   </if_symbolic_assertion>
                                  
                                   <if_numeric_assertion>
                                        <then>
                                                 <text>ATR</text>
                                                 <nassertion_attribute>
                                                        <nassertion_object>
                                                             <attribute_number/>
                                                             <text> OF Obj</text>
                                                             <object_number/>
                                                             <text> := </text>
                                                  <nassertion_make_expression add="+" sub="-" mul="*" div="/" power="exp" neg="-" add_var="infix" sub_var="infix" mul_var="infix" div_var="infix" power_var="prefix" comma="," obracket="(" cbracket=")">
                                                      <if_number>
                                                            <then>
                                                                  <number_value/>
                                                            </then>
                                                      </if_number>
                                                      <if_numeric_value>
                                                           <then>
                                                                 <nv_object>
                                                                     <nv_attribute>
                                                                           <text>ATR</text>
                                                                           <attribute_number/>
                                                                           <text> OF Obj</text>
                                                                           <object_number/>
                                                                     </nv_attribute>
                                                                 </nv_object>
                                                           </then>
                                                      </if_numeric_value>
                                                  </nassertion_make_expression> 
                                                        </nassertion_object>
                                                 </nassertion_attribute>
                                        </then>
                                   </if_numeric_assertion>
                                    <if_assertion_last type="all">
                                          <then/>
                                          <else>
                                                <text>
AND </text>
                                          </else>
                                    </if_assertion_last>
                             </rule_loop_asser_else>
                             <text>

</text>
       </loop_rules>
       
        <text>
END</text>
   </part>
</format_file>
