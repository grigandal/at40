<format_file>
<part file_ext="rss" encoding="dos">
  <comment> Это форматный файл для преобразования поля знаний в БЗ системы GURU</comment>
  <comment> Автор: Иващенко М.Г.</comment>
  <comment> 25.08.2002 </comment>

  <text>GOAL:   GOALME
INITIAL: GOALME=unknown
FORM START
   at 1,1 to 25,80 put border "FABC" with "D"
   at 1,1 to 25,80 put "FABC"
ENDFORM

       e.lstr=80
</text>
   <loop_objects>
      <object_loop_attributes>
         <text>    A</text>
         <object_number/>
         <text>N</text>
         <attribute_number/>
         <text>=unknown
</text>
      </object_loop_attributes>
   </loop_objects>
   <loop_types>
      <if_type_symbolic>
         <then>
            <text>
    S</text>
            <type_number/>
            <text>="</text>
            <type_comment/>
            <text>"
    dim T</text>
            <type_number/>
            <text>(</text>
            <type_loop_values>
               <if_type_value_last>
                  <then>
                     <type_value_number/>
                  </then>
               </if_type_value_last>
            </type_loop_values>
            <text>)
</text>
            <type_loop_values>
            <text>    T</text>
            <type_number/>
            <text>(</text>
            <type_value_number/>
            <text>)="</text>
            <type_value/>
            <text>"
</text>
            </type_loop_values>
            <text>
</text>
         </then>
         <else>
            <message type="info">Предупреждение 1.
Преобразование может содержать ошибки!
 В поле знаний присутствует не символьный тип.</message>
         </else>
      </if_type_symbolic>
   </loop_types>

   <text>DO:
    at 1,1 output "Рекомендация системы: "
    at 1,23 output GOALME

</text>

<loop_rules>
   <text>RULE: R</text>
   <rule_number/>
   <text>
    IF: 
</text>
   <rule_loop_asser_if>
      <if_symbolic_assertion>
          <then>
             <sassertion_attribute>
                <attribute_master>
                   <text>         A</text>
                   <object_number/>
                   <text>N</text>
                   <attribute_number/>
                   <text>=</text>
                   <attribute_type>
                     <type_value_number_equal>
                        <sassertion_value/>
                     </type_value_number_equal>
                   </attribute_type>
                </attribute_master>
             </sassertion_attribute>
             <if_assertion_last type="symbolic">
                 <then>
                   <text>
</text>
                 </then>
                 <else>
                    <text> AND
</text>
                 </else>
             </if_assertion_last>
          </then>
          <else>
            <message type="info">Предупреждение 2.
Преобразование может содержать ошибки!
 В посылке правила присутствует не символьное утверждение.</message>
          </else>
      </if_symbolic_assertion>
   </rule_loop_asser_if>

   <text>    THEN :
</text>
   <rule_loop_asser_then>
      <if_symbolic_assertion>
          <then>
             <sassertion_attribute>
                <attribute_master>
                   <text>         A</text>
                   <object_number/>
                   <text>N</text>
                   <attribute_number/>
                   <text>=</text>
                   <attribute_type>
                     <type_value_number_equal>
                        <sassertion_value/>
                     </type_value_number_equal>
                     <text>
         GOALME=T</text>
                     <type_number/>
                     <text>(A</text>
                     <object_number/>
                     <text>N</text>
                     <attribute_number/>
                     <text>)
</text>
                   </attribute_type>
                </attribute_master>
             </sassertion_attribute>
          </then>
          <else>
            <message type="info"> Предупреждение 3.
Преобразование может содержать ошибки!
 В действии правила присутствует не символьное утверждение.</message>
          </else>
      </if_symbolic_assertion>
   </rule_loop_asser_then>
</loop_rules>

<loop_objects>
  <object_loop_attributes>
      <text>

    VAR: A</text>
      <object_number/>
      <text>N</text>
      <attribute_number/>
      <text>
    FIND: clear
      putform START
                at 2,2 output S</text>
      <attribute_type>
         <type_number/>
         <text>
      A</text>
         <object_number/>
         <text>N</text>
         <attribute_number/>
         <text>=menu(T</text>
         <type_number/>
         <text>,1,</text>
         <if_type_symbolic>
            <then>
                <type_loop_values>
                   <if_type_value_last>
                      <then>
                         <type_value_number/>
                      </then>
                   </if_type_value_last>
                </type_loop_values>
            </then>
            <else>
               <message type="info"> Предупреждение 4. 
Преобразование может содержать ошибки!
 Поле знаний содержит атрибут не символьного типа.</message>
               <text>0</text>
            </else>
         </if_type_symbolic>
      </attribute_type>
      <text>,3,2,0,79,1)</text>
   </object_loop_attributes>
</loop_objects>
<text>

END:</text>
</part>
</format_file>
