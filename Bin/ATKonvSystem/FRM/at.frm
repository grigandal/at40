﻿
<format_file>
<part file_ext="kbs" encoding="">
<comment> Это форматный файл для преобразования поля знаний в БЗ инструментального комплекса АТ-ТЕХНОЛОГИЯ </comment>
<comment> Автор: Иващенко М.Г. </comment>
<comment> 25.08.2002 </comment>

<comment> Здесь описываются типы</comment>

   <loop_types>
     <text>ТИП </text>
     <type_name/>
     <text>
</text>
     <if_type_symbolic>
	<then>
    	   <text>СИМВОЛ
</text>
	   <type_loop_values>
	   <text>"</text>
 	   <type_value/>
	   <text>"
</text>
	   </type_loop_values>
	</then>
     </if_type_symbolic>
     <if_type_numeric>
	<then>
  	   <text>ЧИСЛО
</text>
	   <text>ОТ </text>
	   <type_min/>
 	   <text>
</text>
	   <text>ДО </text>
	   <type_max/>
	   <text>
</text>
	</then>
     </if_type_numeric>
     <if_type_fuzzy>
	<then>
	   <text>НЕЧЕТКИЙ
</text>
	   <type_lt>
	      <text>"</text>
	      <linguistic_name/>
	      <text>"
</text>
	   </type_lt>
	</then>
     </if_type_fuzzy>
     <text>КОММЕНТАРИЙ </text>
     <type_comment/>
     <text>

</text>
   </loop_types>

<comment> Здесь описываются объекты</comment>

   <loop_objects>
      <text>ОБЪЕКТ </text>
      <object_name/>
      <change>
         <comment/>
         <comment/>
         <comment/>
   	 <first/>
 	 <equalfirst>
             <object_group/>
	 </equalfirst>
	 <then/>
	 <else>
	    <text>
ГРУППА </text>
	    <object_group/>
	 </else>
      </change>
      <text>
АТРИБУТЫ
</text>
      <object_loop_attributes>
          <text>АТРИБУТ </text>
	  <attribute_name/>
	  <text>
ТИП </text>
	  <attribute_type>
	     <type_name/>
	  </attribute_type>
	  <text>
КОММЕНТАРИЙ </text>
	  <attribute_comment/>
	  <text>
</text>
      </object_loop_attributes>
      <text>КОММЕНТАРИЙ </text>
      <object_comment/>
      <text>

</text>
   </loop_objects>


<comment> Здесь описываются правила</comment>

   <loop_rules>
      <text>ПРАВИЛО </text>
      <rule_name/>
      <text>
ЕСЛИ
</text>
       <rule_make_logic_value and=" &#38; " or=" | " xor=" XOR " not="~" and_var="infix" or_var="infix" xor_var="infix" comma="," obracket="(" cbracket=")">
          <separator>
</separator>
          <if_object_assertion>
	     <then>
		<oassertion_relation>
		   <relation_name/>
		</oassertion_relation>
		<text>( </text>
		<oassertion_left>
		   <object_name/>
		</oassertion_left>
		<text>, </text>
		<oassertion_right>
		   <object_name/>
		</oassertion_right>
		<text>)</text>
	    </then>
	 </if_object_assertion>
	 <if_numeric_assertion>
	    <then>
		<text>( </text>
		<nassertion_object>
		   <object_name/>
		</nassertion_object>
		<text>.</text>
		<nassertion_attribute>
		   <attribute_name/>
		</nassertion_attribute>
		<nassertion_operation/>
		<text>"</text>
		<nassertion_make_expression add="+" sub="-" mul="*" div="/" power="^" neg="-" add_var="infix" sub_var="infix" mul_var="infix" power_var="infix" div_var="infix" comma="," obracket="(" cbracket=")">
		   <if_number>
		      <then>
			<number_value/>
		      </then>
		   </if_number>
		   <if_numeric_value>
		      <then>
			<nv_object>
			   <object_name/>
			</nv_object>
			<text>.</text>
			<nv_attribute>
			   <attribute_name/>
			</nv_attribute>
		      </then>
		   </if_numeric_value>
		</nassertion_make_expression>
		<text>" </text>
		<assertion_loop_properties>
		   <if_prop_interval>
		      <then>
		 	 <text> УВЕРЕННОСТЬ [</text>
			 <pi_belief_value/>
			 <text> ; </text>
			 <pi_probability_value/>
			 <text>]</text>
		      </then>
		   </if_prop_interval>
		</assertion_loop_properties>
		<assertion_loop_properties>
		   <if_prop_accuracy>
		      <then>
		         <text> ТОЧНОСТЬ </text>
			 <pa_value/>
		      </then>
		   </if_prop_accuracy>
		</assertion_loop_properties>
		<text> )</text>
	    </then>
	 </if_numeric_assertion>
	 <if_symbolic_assertion>
	    <then>
		<text>( </text>
		<sassertion_object>
	 	   <object_name/>
		</sassertion_object>
		<text>.</text>
		<sassertion_attribute>
		   <attribute_name/>
		</sassertion_attribute>
		<text>="</text>
		<sassertion_value/>
		<text>" </text>
		<assertion_loop_properties>
		   <if_prop_interval>
		      <then>
			 <text> УВЕРЕННОСТЬ [</text>
			 <pi_belief_value/>
			 <text> ; </text>
			 <pi_probability_value/>
			 <text>]</text>
		      </then>
		   </if_prop_interval>
		</assertion_loop_properties>
		<assertion_loop_properties>
		   <if_prop_accuracy>
		      <then>
			 <text> ТОЧНОСТЬ </text>
			 <pa_value/>
		      </then>
		   </if_prop_accuracy>
		</assertion_loop_properties>
		<text> )</text>
	    </then>
	 </if_symbolic_assertion>
       </rule_make_logic_value>
       <text>
ТО
</text>
       <rule_loop_asser_then>
 	  <if_object_assertion>
	     <then>
		<oassertion_relation>
		   <relation_name/>
		</oassertion_relation>
		<text>( </text>
		<oassertion_left>
		   <object_name/>
		</oassertion_left>
		<text>, </text>
		<oassertion_right>
		   <object_name/>
		</oassertion_right>
		<text>)
</text>
	     </then>
	 </if_object_assertion>
	 <if_numeric_assertion>
	    <then>
	       	<nassertion_object>
		   <object_name/>
		</nassertion_object>
		<text>.</text>
		<nassertion_attribute>
		   <attribute_name/>
		</nassertion_attribute>
		<nassertion_operation/>
		<text>"</text>
		<nassertion_make_expression add="+" sub="-" mul="*" div="/" power="^" neg="-" add_var="infix" sub_var="infix" mul_var="infix" power_var="infix" div_var="infix" comma="," obracket="(" cbracket=")">
		   <if_number>
		      <then>
			 <number_value/>
		      </then>
		   </if_number>
		   <if_numeric_value>
		      <then>
			 <nv_object>
			    <object_name/>
			 </nv_object>
			 <text>.</text>
			 <nv_attribute>
			    <attribute_name/>
			 </nv_attribute>
		      </then>
		   </if_numeric_value>
		</nassertion_make_expression>
		<text>"</text>
		<assertion_loop_properties>
		   <if_prop_interval>
		      <then>
		 	 <text> УВЕРЕННОСТЬ [</text>
			 <pi_belief_value/>
			 <text> ; </text>
			 <pi_probability_value/>
			 <text>]</text>
		      </then>
		   </if_prop_interval>
		</assertion_loop_properties>
		<assertion_loop_properties>
		   <if_prop_accuracy>
		      <then>
		 	 <text> ТОЧНОСТЬ </text>
			 <pa_value/>
		      </then>
		   </if_prop_accuracy>
		</assertion_loop_properties>
		<text>
</text>
	    </then>
	 </if_numeric_assertion>
	 <if_symbolic_assertion>
	    <then>
		<sassertion_object>
		   <object_name/>
		</sassertion_object>
		<text>.</text>
		<sassertion_attribute>
		   <attribute_name/>
		</sassertion_attribute>
		<text>="</text>
		<sassertion_value/>
		<text>"</text>
		<assertion_loop_properties>
		   <if_prop_interval>
		      <then>
			 <text> УВЕРЕННОСТЬ [</text>
			 <pi_belief_value/>
			 <text> ; </text>
			 <pi_probability_value/>
			 <text>]</text>
		      </then>
		   </if_prop_interval>
		</assertion_loop_properties>
		<assertion_loop_properties>
		   <if_prop_accuracy>
		      <then>
			 <text> ТОЧНОСТЬ </text>
			 <pa_value/>
		      </then>
		   </if_prop_accuracy>
		</assertion_loop_properties>
		<text>
</text>
	    </then>
	 </if_symbolic_assertion>
       </rule_loop_asser_then>
       <if_rule_has_else>
	  <then>
	     <text>ИНАЧЕ
</text>
	     <rule_loop_asser_else>
		<if_object_assertion>
	   	   <then>
		      <oassertion_relation>
			  <relation_name/>
		      </oassertion_relation>
		      <text>( </text>
		      <oassertion_left>
			  <object_name/>
		      </oassertion_left>
		      <text>, </text>
		      <oassertion_right>
			  <object_name/>
		      </oassertion_right>
		      <text>)
</text>
		  </then>
		</if_object_assertion>
		<if_numeric_assertion>
		   <then>
		      <nassertion_object>
			  <object_name/>
		      </nassertion_object>
		      <text>.</text>
		      <nassertion_attribute>
			  <attribute_name/>
		      </nassertion_attribute>
		      <nassertion_operation/>
		      <text>"</text>
		      <nassertion_make_expression add="+" sub="-" mul="*" div="/" power="^" neg="-" add_var="infix" sub_var="infix" mul_var="infix" power_var="infix" div_var="infix" comma="," obracket="(" cbracket=")">
		 	 <if_number>
			    <then>
			       <number_value/>
			    </then>
			 </if_number>
			 <if_numeric_value>
			    <then>
			       <nv_object>
			 	  <object_name/>
			       </nv_object>
			       <text>.</text>
			       <nv_attribute>
			 	  <attribute_name/>
			       </nv_attribute>
			    </then>
			 </if_numeric_value>
		      </nassertion_make_expression>
		      <text>"</text>
		      <assertion_loop_properties>
			 <if_prop_interval>
			    <then>
			       <text> УВЕРЕННОСТЬ [</text>
			       <pi_belief_value/>
			       <text> ; </text>
			       <pi_probability_value/>
			       <text>]</text>
			    </then>
			 </if_prop_interval>
		      </assertion_loop_properties>
		      <assertion_loop_properties>
			 <if_prop_accuracy>
			    <then>
			       <text> ТОЧНОСТЬ </text>
			       <pa_value/>
			    </then>
			 </if_prop_accuracy>
		      </assertion_loop_properties>
		      <text>
</text>
		   </then>
		</if_numeric_assertion>
		<if_symbolic_assertion>
		   <then>
		      <sassertion_object>
			 <object_name/>
		      </sassertion_object>
		      <text>.</text>
		      <sassertion_attribute>
			 <attribute_name/>
		      </sassertion_attribute>
		      <text>="</text>
		      <sassertion_value/>
		      <text>"</text>
		      <assertion_loop_properties>
			 <if_prop_interval>
			    <then>
			       <text> УВЕРЕННОСТЬ [</text>
			       <pi_belief_value/>
			       <text> ; </text>
			       <pi_probability_value/>
			       <text>]</text>
			    </then>
			 </if_prop_interval>
		      </assertion_loop_properties>
		      <assertion_loop_properties>
		 	 <if_prop_accuracy>
			    <then>
			       <text> ТОЧНОСТЬ </text>
			       <pa_value/>
			    </then>
			 </if_prop_accuracy>
		      </assertion_loop_properties>
		      <text>
</text>
		   </then>
		</if_symbolic_assertion>
	     </rule_loop_asser_else>
	  </then>
       </if_rule_has_else>
       <text>КОММЕНТАРИЙ </text>
       <rule_comment/>
       <text>

</text>
   </loop_rules>
</part>
</format_file>
