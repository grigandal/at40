'***********************************
' VB Script Demo for Script Runner *
'***********************************
Const TITLE = "Script Runner Demo"
Dim CurDir
Dim bb

Sub SetGlobals(aCurDir, aBB)
  CurDir = aCurDir
  bb = aBB
  Msgbox aCurDir
End Sub

Sub SetCurDir(aCurDir)
  CurDir = aCurDir
  Msgbox CurDir
End Sub


Sub Main()
Dim MyDate

  Select Case Msgbox("Hello! Do you want to know the Date ?", _
                     vbYesNo + vbQuestion, TITLE)

    Case vbYes
      GetDate MyDate
      If Msgbox("Today is " & MyDate & vbCrLf & "And also want to know the time ?" & _
                vbCrLf & "Dont say no or I will Raise an Exception !!!", _
                vbYesNo + vbExclamation + vbDefaultButton2, TITLE) = vbYes then
        Msgbox "The time is " & GetTime(), vbInformation, TITLE     
        Msgbox "See ya!!"    
      Else
        Err.Raise 1000, "Script Runner Demo", "Refuse to get time error :)"
      End If

    Case vbNo
      Call Msgbox("Bye!!", vbInformation, TITLE) 

  End Select
End Sub

Sub GetDate(X)
  X = CStr(Date())
  MsgBox X, vbInformation, TITLE
End Sub

Function GetTime()
  GetTime = CStr(Time())
End Function

Function GetArray()
  Dim Arr(2)
  Arr(0) = "dima"
  Arr(1) = 24
  Arr(2) = CStr(Time()) 
  GetArray = Arr
End Function

Function Sum(a, b)
  Dim arr As Variant
  MsgBox a, vbInformation, TITLE
  MsgBox b, vbInformation, TITLE
  Sum = a+b
  Msgbox aCurDir
End Function

Sub Form(diagnosis, orz, flue)
  Dim App
  Set App = CreateObject("Excel.Application")  
  'App.Workbooks.Add()
  App.Workbooks.Open(CurDir + "\report.xls")

  App.Range("A1").Select
  App.ActiveCell.Value = "�������"
  App.Range("B1").Select  
  App.ActiveCell.Value = diagnosis
  
  App.Range("A2").Select
  App.ActiveCell.Value = "���"
  App.Range("B2").Select  
  App.ActiveCell.Value = orz

  App.Range("A3").Select
  App.ActiveCell.Value = "�����"
  App.Range("B3").Select  
  App.ActiveCell.Value = flue

  App.Visible = true

  'App.Quit
End Sub

Sub UseMCAD()
  Dim App
  Set App = CreateObject("Mathcad.Application")
  App.Visible = true
  App.Worksheets.Open CurDir+"gravity.mcd"
  App.ActiveWorksheet.Recalculate()

  Dim xGr1
  Set xGr1 = App.ActiveWorksheet.GetValue("xGr1")
  Dim yGr1
  Set yGr1 = App.ActiveWorksheet.GetValue("yGr1")
  Msgbox "������ �� �������! " + Chr(13) + "���������� xGr1 ����� ��� " + xGr1.Type + " � �������� " + xGr1.AsString + Chr(13) + "yGr1 = " + yGr1.AsString
    
  'Dim asdf As Variant

  App.Quit

End Sub

